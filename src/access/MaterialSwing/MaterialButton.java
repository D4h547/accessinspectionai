package access.MaterialSwing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class MaterialButton extends JButton {

    private Dimension dimenIngComp = new Dimension(165, 26);
    private static final Font FONT_COMBOBOX = new Font("apple-system", Font.BOLD, 12);

    public MaterialButton(String string) {
        super();
        InitComponentButton(string);
    }

    private void InitComponentButton(String string) {
        setBackground(MaterialColor.GRAY_200);
        setPreferredSize(this.dimenIngComp);
        setText(string);
        setFont(FONT_COMBOBOX);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(MaterialColor.GRAY_200);
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(MaterialColor.GRAY_500);
            }
        });
    }
}
