package access.MaterialSwing;

import java.awt.Color;

public class MaterialColor {

    /**
     * Material Design Color palette.
     *
     * @see
     * <a href="https://www.google.com/design/spec/style/color.html#color-ui-color-palette">Color
     * (Google design guidelines)</a>
     */

    //public static final Color COLOR_FONDO_MARCO = new Color(52, 58, 64); 
    //public static final Color COLOR_FONDO_MENU = new Color(190, 31, 31);
    //public static final Color COLOR_FONDO_BASE = new Color(202, 199, 199);
    //public static final Color COLOR_FONT = new Color(255, 255, 255);
    //public static final Color COLOR_BORDER_JTEXTFIELD = new Color(0, 123, 255);
    //public static final Color COLOR_FONT_MSG = new Color(251, 146, 27);
    //public static final Color COLOR_FONDO_BTN = new Color(202, 199, 199); // gris claro
    //public static final Color COLOR_FONDO_BTN_HOVER = new Color(160, 159, 159); // gris mas oscuro
    
    public static final Color WHITH = Color.decode("#FFFFFF"); //COLOR_FONT
    public static final Color BLACK = Color.decode("#050505"); //COLOR_FONT
    
    public static final Color GRAY_200 = Color.decode("#EEEEEE"); //COLOR_FONDO_BTN
    public static final Color GRAY_300 = Color.decode("#E0E0E0"); //COLOR_FONDO_BTN
    public static final Color GRAY_400 = Color.decode("#BDBDBD"); //COLOR_FONDO_BTN
    
    public static final Color GRAY_500 = Color.decode("#9E9E9E"); //COLOR_FONDO_BTN_HOVER
    public static final Color GRAY_600 = Color.decode("#757575"); //COLOR_FONDO_BTN_HOVER
    
    public static final Color DEEP_ORANGE_600 = Color.decode("#F4511E"); //COLOR_FONT_MSG_ERROR
    public static final Color DEEP_ORANGE_700 = Color.decode("#E64A19"); //COLOR_FONT_MSG_ERROR
    public static final Color DEEP_ORANGE_800 = Color.decode("#D84315"); //COLOR_FONT_MSG_ERROR
    
    public static final Color LIGHTBLUE_800 = Color.decode("#0277bd"); // COLOR_BORDER_JTEXTFIELD_CLARO
    public static final Color LIGHTBLUE_900 = Color.decode("#01579b"); // COLOR_BORDER_JTEXTFIELD
    
    public static final Color RED_800 = Color.decode("#C62828"); //COLOR_FONDO_MENU_CLARO
    public static final Color RED_900 = Color.decode("#B71C1C"); //COLOR_FONDO_MENU
    
    public static final Color BLUEGRAY_800 = Color.decode("#343A40"); // COLOR_FONDO_MARCO
    public static final Color BLUEGRAY_900 = Color.decode("#263238"); // COLOR_FONDO_MARCO
} 