
package access.MaterialSwing;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JPanel;

public class MaterialPanelReloj extends JPanel{

    public MaterialPanelReloj() {
        InitComponent();
    }
    private void InitComponent(){
        rSLabelHora1 = new rojeru_san.RSLabelHora();
        rSLabelHora1.setPreferredSize(new Dimension(315,40));
        rSLabelHora1.setForeground(MaterialColor.WHITH);
        rSLabelHora1.setFont(new java.awt.Font("Roboto Bold", 1, 40)); // NOI18N
        this.setBackground(MaterialColor.BLUEGRAY_900);
        this.add(rSLabelHora1);
    }
    private rojeru_san.RSLabelHora rSLabelHora1;
}
