package access.MaterialSwing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MaterialPanelMenu extends JPanel {

    //Dimension tamPantalla = new Dimension(getToolkit().getScreenSize());
    private static final Font FONT_TEXT_TITLE_MENU = new Font("apple-system", Font.BOLD, 50);
    int whidth = 980;
    private JLabel imgLogoAccess;
    private JLabel accessTitulo;

    public MaterialPanelMenu() {
        super();
        InitComponentPanel();
    }

    private void InitComponentPanel() {

        Toolkit pantalla = Toolkit.getDefaultToolkit();
        Dimension tamanioPan = pantalla.getScreenSize();

        // resolucion de la pantalla
        int anPan = tamanioPan.width;

        imgLogoAccess = new JLabel();
        ImageIcon logoAccess = new ImageIcon(getClass().getResource("/img/huellaBlanca.png"));
        ImageIcon icoAccess = new ImageIcon(logoAccess.getImage().getScaledInstance(100, 90, Image.SCALE_DEFAULT));
        imgLogoAccess.setIcon(icoAccess);
        imgLogoAccess.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));

        accessTitulo = new JLabel("ACCESS INSPECTION");
        accessTitulo.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
        accessTitulo.setForeground(MaterialColor.WHITH);
        accessTitulo.setFont(FONT_TEXT_TITLE_MENU);
        
        setPreferredSize(new Dimension(anPan, 100));
        setLayout(new BorderLayout());
        setBackground(MaterialColor.RED_900);
        add(imgLogoAccess, BorderLayout.WEST);
        add(accessTitulo, BorderLayout.CENTER);
    }

    public void setResizeTamaMenu(int whidth) {
        this.whidth = whidth;
        setPreferredSize(new Dimension(whidth, 100));
        updateUI();
        //System.out.println("whidth " + whidth + "\n heigth: " + height);
    }
}
