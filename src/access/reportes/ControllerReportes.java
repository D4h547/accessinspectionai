package access.reportes;

import access.conexionDao.DaoConexion;
import access.conexionDao.DaoReportes;
import access.model.ModelReportDiario;
import access.model.ModelReportQuin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControllerReportes {

    Calendar calendar;
    Calendar clDia;
    // fecha actual pasa la fecha y tambien la hora de creacion del reporte
    Date fechaActual;
    Date fechaIni;
    Date fechaFin;
    Date fechaDiario;

    DaoConexion conn = new DaoConexion();
    DaoReportes dr = new DaoReportes(conn);
    
    ModelReportQuin mdlReQuin = new ModelReportQuin();
    ModelReportDiario modelReDia = new ModelReportDiario();

    public ControllerReportes(Date fecha) {
        this.fechaActual = fecha;
    }

    public void setReporteQuinDiario() {
        if (dr.CheckFechaFin(fechaActual)) {
            // toma dia actual
            this.calendar = Calendar.getInstance();
            // suma un dia al dia actual
            this.calendar.add(Calendar.DATE, 1);
            // toma la fecha con un dia adelantado
            this.fechaIni = calendar.getTime();
            // suma 14 dias para declarar la fecha fin de quincena
            this.calendar.add(Calendar.DATE, 15);
            // toma la fecha con 15 sumados
            this.fechaFin = calendar.getTime();
            SendDatesInsert();
            System.out.println("Se crea un nuevo reporte");
        } else {
            //System.out.println("No se crea un nuevo reporte");
        }

        clDia = Calendar.getInstance();
        clDia.add(Calendar.DATE, 1);
        fechaDiario = clDia.getTime();
        SetDatosReporDay(fechaDiario);
        System.out.println("Dia Siguiente");

    }

    private void SendDatesInsert() {
        mdlReQuin.setFechaInit(fechaIni);
        mdlReQuin.setFechaFin(fechaFin);
        mdlReQuin.setHora(fechaIni);
        if (dr.InsertReportQuincenal(mdlReQuin)) {
            System.out.println("se creo el reporte quincenal");
        } else {
            System.out.println("no se creo el reporte quincenal");
        }
    }

    public void SetDatosReporDay(Date dia) {
        int id = dr.SelecIdReportQinc(dia);
        if (id != 0) {
            modelReDia.setFechaRepDia(dia);
            modelReDia.setHora(dia);
            modelReDia.setIdReporQin(id);
            if (dr.InsertReportDiario(modelReDia)) {
                System.out.println("Se Genero el Reporte Diario");
            } else {
                System.out.println("No se genero el reporte diario");
            }
        } else {
            System.out.println("Se ejecuta pero no entra al fin");
        }
    }

    private boolean ValidarReportQuin2(Date dia) {
        System.out.println(dia);
        if (dr.CheckFechaFin(dia)) {
            System.out.println("Se crea un nuevo reporte");
            return true;
        } else {
            System.out.println("No se crea un nuevo reporte");
            return false;
        }
    }

    private void SendDatesInsert2(Date diaIni, Date diaFin) {
        mdlReQuin.setFechaInit(diaIni);
        mdlReQuin.setFechaFin(diaFin);
        mdlReQuin.setHora(diaIni);
        if (dr.InsertReportQuincenal(mdlReQuin)) {
            System.out.println("se creo el reporte quincenal");
        } else {
            System.out.println("no se creo el reporte quincenal");
        }
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public boolean ValidReportDiario(Date fechaDia) {
        boolean exists = false;
        String sql = "SELECT fecha_reporte FROM reporte_diario WHERE fecha_reporte = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(fechaDia));
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
        }

    private void pruebas() {
        Calendar dia = Calendar.getInstance();
        Calendar diaFin2 = Calendar.getInstance();
        Calendar diaIni2 = Calendar.getInstance();
        Date dia2;
        //this.calendar = Calendar.getInstance();
        for (int e = 1; e <= 200; e++) {

            dia2 = dia.getTime();

            if (ValidarReportQuin2(dia2)) {

                diaIni2.setTime(dia2);
                diaIni2.add(Calendar.DATE, 1);
                Date diaIni = diaIni2.getTime();

                diaFin2.setTime(dia2);
                diaFin2.add(Calendar.DATE, 15);
                Date diaFin = diaFin2.getTime();

                SendDatesInsert2(diaIni, diaFin);
            }
            SetDatosReporDay(dia2);
            dia.add(Calendar.DATE, 1);
            //System.out.println(dia2);
        }
    }

    private void pruebas2() {
        Calendar dia = Calendar.getInstance();
        Calendar diaFin2 = Calendar.getInstance();
        Calendar diaIni2 = Calendar.getInstance();
        Date dia2;
        //this.calendar = Calendar.getInstance();
        for (int e = 1; e <= 200; e++) {

            dia2 = dia.getTime();

            if (ValidarReportQuin2(dia2)) {

                diaIni2.setTime(dia2);
                diaIni2.add(Calendar.DATE, 1);
                Date diaIni = diaIni2.getTime();

                diaFin2.setTime(dia2);
                diaFin2.add(Calendar.DATE, 15);
                Date diaFin = diaFin2.getTime();

                SendDatesInsert2(diaIni, diaFin);
            }
            SetDatosReporDay(dia2);
            dia.add(Calendar.DATE, 1);
            //System.out.println(dia2);
        }
    }
}

/**
 * calendar.add(Calendar.DATE, 1); fechaActual = calendar.getTime();
 * System.out.println("Inicio Reporte No:"+i+" : "+fechaActual);
 * calendar.add(Calendar.DATE, 14); fechaFin = calendar.getTime();
 * System.out.println("Finnal Reporte No:"+i+" : "+fechaFin);
 *
 */
