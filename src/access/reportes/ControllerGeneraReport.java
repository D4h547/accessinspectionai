package access.reportes;

import access.conexionDao.DaoConexion;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ControllerGeneraReport {

    Runnable runnable;
    ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    Calendar cl = Calendar.getInstance();
    private final ControllerReportes setReport;

    public ControllerGeneraReport() {
        Date diaActual = cl.getTime();
        this.setReport = new ControllerReportes(diaActual);
        DelayInicial();
        Iniciar();
        if(!setReport.ValidReportDiario(diaActual)){
            setReport.SetDatosReporDay(diaActual);
        }
    }
    private void Iniciar() {
        runnable = new Runnable() {
            @Override
            public void run() {
                setReport.setReporteQuinDiario();
            }
        };
        service.scheduleWithFixedDelay(runnable, minuDelay, horaRepor, TimeUnit.MINUTES);
        //service.scheduleWithFixedDelay(runnable, 0, 1, TimeUnit.MINUTES);
    }
    
    // tiempo para iniciar la primera creacion del reporte en minutos
    int minuDelay = 1;
    // tiempo para esperar la sguiente creacion del reporte en minutos
    int horaRepor = (23 * 60) + 10;

    private void DelayInicial() {
        
        // pasamos las 23 hora a minutos y le sumamos 10 minutos (hora: 23:10)
        LocalTime horaActual = LocalTime.now();
        //LocalTime horaActual = LocalTime.of(23, 43);
        LocalTime horaCreaRepor = LocalTime.of(23, 10);
        LocalTime logHora;
        if (horaActual.isBefore(horaCreaRepor)) {
            //System.out.println("hora actual: " + horaActual);
            // tomo la hora en que se cera el reporte y le resto los minutos de la hora actual
            logHora = horaCreaRepor.minusMinutes(horaActual.getMinute());
            // obtengo el tiempo exacto (horas y munutos) que falta que sea la hora de reporte (23:10) 
            logHora = logHora.minusHours(horaActual.getHour());
            //System.out.println("faltan: " + logHora);
            int horas = logHora.getHour();
            //System.out.println("Hora: " + horas);
            //System.out.println("Minutos: " + logHora.getMinute());
            minuDelay = (horas * 60) + logHora.getMinute();
            //System.out.println("Total delay Inicial: " + minuDelay);
        } else {
            minuDelay = 1;
        }
    }
}
