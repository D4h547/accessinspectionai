package access.controller;

import access.model.ModelEmpleado;
import access.model.ModelLogin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControllerVerify {

    String textNull = "Este campo no puede estar vacio";
    String textMuch = "Tamaño Excedido";
    String msg = "";

    public boolean validateText150(String string) {
        boolean status = false;
        if (string.equals("")) {
            msg = textNull;
        } else {
            if (string.length() <= 150) {
                status = true;
            } else {
                msg = textMuch;
            }
        }
        return status;
    }

    public boolean VerifyString(String string) {
        boolean status = false;
        String exString = "[A-Za-z]{3,140}";
        Pattern pattern = Pattern.compile(exString);
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            //System.out.println("Nombre Valido");
            status = true;
        }
        return status;
    }

    public boolean VerifyNumbers(String string) {
        boolean status = false;
        String exString = "[0-9]";
        Pattern pattern = Pattern.compile(exString);
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            //System.out.println("Nombre Valido");
            status = true;
        }
        return status;
    }

    public String RemoveCharNotValidString(String string) {
        string = string.replaceAll("[^a-zA-Z0-9# -]", "");
        if (string.length() > 150) {
            System.out.println("Direccion muy Grande para Guardar en la base de datos");
        } else {
            System.out.println(string);
        }
        return string;
    }

    public boolean VerifyCorreo(String string) {
        String strCorreo = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        boolean status = false;
        Pattern pattern = Pattern.compile(strCorreo);
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            //System.out.println("Correo Valido");
            status = true;
        }
        return status;
    }

    ModelEmpleado model = new ModelEmpleado();

    public boolean valiateEmployed(ModelEmpleado empl) {
        return !(  empl.getApellidos() == null || empl.getNombres() == null
                || empl.getTipoDoc() == null  || empl.getNumeroDoc() == null
                || empl.getDtFechaNac() == null  || empl.getRH() == null
                || empl.getDepartamento() == null    || empl.getMunicipio() == null
                || empl.getDireccion() == null    || empl.getCorreo() == null
                || empl.getTelefono() == null || empl.getCargo() == null
                || empl.getFoto() == null);
    }
    
    public boolean validate(ModelLogin loginModel) throws SQLException {
        boolean status = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            //String sqlConsult = "SELECT * FROM administradores WHERE correoAdmin = ? AND password = ?";
            String sqlLogin = "SELECT * FROM administradores AS ad JOIN roles AS r ON (idAdmin=idAdminFk) WHERE ad.correoAdmin = ? and ad.password = ? and r.rol = ?";            
            //ps = conn.getConnetion().prepareStatement(sqlLogin);
            //System.out.println(loginModel.toString());
            ps.setString(1, loginModel.getCorreo());
            ps.setString(2, loginModel.getPassword());
            ps.setString(3, loginModel.getRol());
            rs = ps.executeQuery();
            System.out.println(rs);
            status = rs.next();
            System.out.println(rs.next());
            System.out.println(status);
        } catch (SQLException e) {
            System.out.println("Fallo en la conexion");
            System.out.println(e.getMessage());
        }
        return status;
    }
}
