package access.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControllerFormEmp {

    String textNull = "Este campo no puede estar vacio";
    String textMuch = "Tamaño de nombre no valido";
    String msg = "";

    public boolean validateText150(String string) {
        String exString = "[A-Za-z]";
        Pattern pattern = Pattern.compile(exString);
        Matcher matcher = pattern.matcher(string);
        msg = "";
        boolean status = false;
        if (string.equals("")) {
            msg = textNull;
        } else {
            if (matcher.matches()) {
                //System.out.println("Nombre Valido");
                if (string.length() >=3 && string.length() <= 150) {
                    status = true;
                } else {
                    msg = textMuch;
                }
            } else {
                msg = "escribre solo letras";
            }
        }
        return status;
    }

    public boolean VerifyString(String string) {
        boolean status = false;
        String exString = "[A-Za-z]";
        Pattern pattern = Pattern.compile(exString);
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            //System.out.println("Nombre Valido");
            status = true;
        }
        return status;
    }

    public boolean VerifyNumbers(String string) {
        boolean status = false;
        String exString = "[0-9]";
        Pattern pattern = Pattern.compile(exString);
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            //System.out.println("Nombre Valido");

            status = true;
        }
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
