package access.controller;

import com.google.gson.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControllerDeparMuni {
/**
    public static void main(String[] args) throws FileNotFoundException {
        /**
        List ini = getDepartamentos();
        for (Object departamento : ini) {
            System.out.println(departamento);
        }
        //hola();
        List ini = getCiudades("Tolima");
        for (Object departamento : ini) {
            System.out.println(departamento);
        }
    }**/

    public String[] getDepartamentos() {
        String[] departamentos = null;
        try {
            JsonParser parser = new JsonParser();
            FileReader fileReader = new FileReader("C:\\Users\\sdare\\OneDrive\\Documentos\\NetBeansProjects\\AccessInpectionAI\\src\\utilites\\colombia.json");
            // Obtain Array
            JsonArray arrDepar = parser.parse(fileReader).getAsJsonArray();
            int sizeArr = arrDepar.size();
            departamentos = new String[sizeArr];
            int cont = 0;
            // for each element of array
            for (JsonElement inDepto : arrDepar) {
                // Object of array
                JsonObject obDepar = inDepto.getAsJsonObject();
                String depar = obDepar.get("departamento").getAsString();
                departamentos[cont] = depar;
                cont++;
            }
            //System.out.println(departamentos);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControllerDeparMuni.class.getName()).log(Level.SEVERE, null, ex);
        }
        return departamentos;
    }
    public String[] getCiudades(String pais) {
        String[] ciudades = null;
        try {
            JsonParser parser = new JsonParser();;
            FileReader fileReader;
            
            fileReader = new FileReader("C:\\Users\\sdare\\OneDrive\\Documentos\\NetBeansProjects\\AccessInpectionAI\\src\\utilites\\colombia.json");
            // Obtain Array
            JsonArray arrDepar = parser.parse(fileReader).getAsJsonArray();
            int con = 0;
            // for each element of array
            for (JsonElement inDepto : arrDepar) {
                // Object of array
                JsonObject obDepar = inDepto.getAsJsonObject();
                String depar = obDepar.get("departamento").getAsString();
                JsonArray obCiudades = obDepar.get("ciudades").getAsJsonArray();
                //System.out.println(depar);
                //System.out.println(pais);
                if (pais.equals(depar)) {
                    int sizeArr = obCiudades.size();
                    ciudades = new String[sizeArr];
                    for (JsonElement inCiudad : obCiudades) {
                        String strCiudad = inCiudad.getAsString();
                        ciudades[con] = strCiudad;
                        //System.out.println(inCiudad);
                        con++;
                    }
                    break;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControllerDeparMuni.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ciudades;
    }
/**
    public static void hola() throws FileNotFoundException {
        JsonParser parser = new JsonParser();
        FileReader fileReader = new FileReader("src/json/colombia.json");

        // Obtain Array
        JsonArray gsonArr = parser.parse(fileReader).getAsJsonArray();

        //for each element   of array   {
        for (JsonElement obj : gsonArr) { // Object of array 
            JsonObject gsonObj = obj.getAsJsonObject();
            //Primitives elements of object 
            //int id = gsonObj.get("id").getAsInt();
            //String depar = gsonObj.get("departamento").getAsString();
            //System.out.println("id: " + id);
            //System.out.println("departamento: " + depar); //List of primitive elements
            JsonArray ciudades = gsonObj.get("ciudades").getAsJsonArray(); //List
            //listDemarcation = new ArrayList();
            
            for (JsonElement ciudad : ciudades) {
                //listDemarcation.add(demarc.getAsString()); 
                System.out.println(ciudad);
            }
        }
    }
    **/
}
