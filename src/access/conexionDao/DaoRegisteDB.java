package access.conexionDao;

import access.model.ModelEmpleado;
import access.model.ModelHuellaEmp;
import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoRegisteDB {

    DaoConexion conn;

    public DaoRegisteDB(DaoConexion conn) {
        this.conn = conn;
    }

    public boolean CheckEmpExists(String numDoc) {
        boolean exists = false;
        String querySQL = "SELECT numDoc FROM empleados WHERE numDoc = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(querySQL);
            ps.setString(1, numDoc);
            ResultSet result = ps.executeQuery();
            exists = result.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }

    public boolean InsertFormEmpleado(ModelEmpleado empleado) {

        String insertSQL = "INSERT INTO empleados values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            PreparedStatement insertPS = conn.getConnetion().prepareStatement(insertSQL);
            insertPS.setString(1, empleado.getNumeroDoc());
            insertPS.setString(2, empleado.getNombres());
            insertPS.setString(3, empleado.getApellidos());
            insertPS.setString(4, empleado.getTipoDoc());
            // pasamos la fecha con formato en tipo string
            insertPS.setString(5, formato.format(empleado.getDtFechaNac()));
            insertPS.setString(6, empleado.getRH());
            insertPS.setString(7, empleado.getDepartamento());
            insertPS.setString(8, empleado.getMunicipio());
            insertPS.setString(9, empleado.getDireccion());
            insertPS.setString(10, empleado.getCorreo());
            insertPS.setString(11, empleado.getTelefono());
            insertPS.setString(12, empleado.getCargo());
            insertPS.setString(13, empleado.getFoto());
            insertPS.setString(14, formato.format(empleado.getFechaCreacion()));
            insertPS.setInt(15, empleado.getIdAdminCreater());
            insertPS.executeUpdate();
            //System.out.println("Clase empleado Dao metodo insert");
            //System.out.println(empleado);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println("Error al insertar");
            return false;
        }
    }

    public boolean InsertHuellaEmpleado(ModelHuellaEmp huellaEmp) {
        //Obtiene los datos del template de la huella actual
        ByteArrayInputStream datosHuella = new ByteArrayInputStream(huellaEmp.getTemplate().serialize());
        Integer tamañoHuella = huellaEmp.getTemplate().serialize().length;
        String insertSQl = "INSERT INTO HuellaCode (huella, codigoPin, numDocEmp) values (?,?,?);";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(insertSQl);
            ps.setBinaryStream(1, datosHuella, tamañoHuella);
            ps.setInt(2, huellaEmp.getCodePin());
            ps.setString(3, huellaEmp.getNumDoc());
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
            //Si ocurre un error lo indica en la consola
            //System.err.println("Error al guardar los datos de la huella.");
            return false;
        }
    }

    public boolean ValidateCodePin(int pin) {
        String sqlSelectCodePin = "SELECT codigoPin FROM HuellaCode WHERE codigoPin = ?;";
        boolean exists = false;
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlSelectCodePin);
            ps.setInt(1, pin);
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }

}
