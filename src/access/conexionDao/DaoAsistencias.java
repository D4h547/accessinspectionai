
package access.conexionDao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoAsistencias {
    
    DaoConexion conn;

    public DaoAsistencias(DaoConexion conn) {
        this.conn = conn;
    }
    
    public List SelectDatosEmp(String cc) {
        List empl = new ArrayList();
        String sqlSelect = "SELECT numDoc, nombres, apellidos FROM empleados WHERE numDoc = ?;";
        try {
            PreparedStatement psSelect = conn.getConnetion().prepareStatement(sqlSelect);
            psSelect.setString(1, cc);
            ResultSet rs = psSelect.executeQuery();
            while (rs.next()) {
                empl.add(rs.getString("numDoc"));
                empl.add(rs.getString("nombres"));
                empl.add(rs.getString("apellidos"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empl;
    }

    public boolean ValidateCodePin(int pin) {
        String sqlSelectCodePin = "SELECT codigoPin FROM HuellaCode WHERE codigoPin = ?;";
        boolean exists = false;
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlSelectCodePin);
            ps.setInt(1, pin);
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }
    
    public boolean InsertHoraEntrada(String hora, String fecha, String cc) {
        boolean status = false;
        String sqlInsertHoraEntrada = "INSERT INTO asistencia_entrada (hora_entrada, fecha_e_fk, documento) VALUES (?,?,?);";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlInsertHoraEntrada);
            ps.setString(1, hora);
            ps.setString(2, fecha);
            ps.setString(3, cc);
            ps.executeUpdate();
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public boolean InsertHoraSalida(String hora, String fecha, String cc) {
        boolean status = false;
        String sqlInsertHoraEntrada = "INSERT INTO asistencia_salida (hora_salida, fecha_s_fk, documento_fk) VALUES (?,?,?);";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlInsertHoraEntrada);
            ps.setString(1, hora);
            ps.setString(2, fecha);
            ps.setString(3, cc);
            ps.executeUpdate();
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public boolean CheckHourEntrada(String cc, String fecha) {
        boolean exists = false;
        String sqlCheckEntrada = "SELECT hora_entrada FROM asistencia_entrada WHERE documento = ? and fecha_e_fk = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlCheckEntrada);
            ps.setString(1, cc);
            ps.setString(2, fecha);
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }

    public boolean CheckHourSalida(String cc, String fecha) {
        boolean exists = false;
        String sqlCheckEntrada = "SELECT hora_salida FROM asistencia_salida WHERE documento_fk = ? and fecha_s_fk = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareStatement(sqlCheckEntrada);
            ps.setString(1, cc);
            ps.setString(2, fecha);
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoRegisteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }

}
