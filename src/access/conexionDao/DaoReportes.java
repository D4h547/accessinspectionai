package access.conexionDao;

import access.model.ModelReportDiario;
import access.model.ModelReportQuin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoReportes {

    DaoConexion conn;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");

    public DaoReportes(DaoConexion conn) {
        this.conn = conn;
    }

    public boolean CheckFechaFin(Date fechaFin) {
        boolean valid = false;
        String sql = "SELECT fecha_report_quince_fin FROM reporte_quincenal WHERE fecha_report_quince_fin = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(fechaFin));
            //System.out.println(dateFormat.format(fechaFin));
            ps.executeQuery();
            ResultSet rs = ps.executeQuery();
            valid = rs.next();
            //return true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
            //return false;
        }
        return valid;
    }

    public boolean InsertReportQuincenal(ModelReportQuin mdlReQuin) {
        String sql = "INSERT INTO reporte_quincenal (fecha_report_quince_inicio,fecha_report_quince_fin,hora_report_quince) VALUES (?,?,?);";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(mdlReQuin.getFechaInit()));
            ps.setString(2, dateFormat.format(mdlReQuin.getFechaFin()));
            ps.setString(3, timeFormat.format(mdlReQuin.getHora()));
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public int SelecIdReportQinc(Date fechaFin) {
        int id = 0;
        String sql = "SELECT id_rp_quincenal FROM reporte_quincenal WHERE ? BETWEEN fecha_report_quince_inicio and fecha_report_quince_fin;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(fechaFin));
            //System.out.println(dateFormat.format(fechaFin));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id_rp_quincenal");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public boolean InsertReportDiario(ModelReportDiario mdlReDia) {
        String sql = "INSERT INTO reporte_diario(fecha_reporte,hora_reporte,id_rp_quincenal_kf) VALUES (?,?,?);";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(mdlReDia.getFechaRepDia()));
            ps.setString(2, timeFormat.format(mdlReDia.getHora()));
            ps.setInt(3, mdlReDia.getIdReporQin());
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean ValidReportDiario(Date fechaDia) {
        boolean exists = false;
        String sql = "SELECT fecha_reporte FROM reporte_diario WHERE fecha_reporte = ?;";
        try {
            PreparedStatement ps = conn.getConnetion().prepareCall(sql);
            ps.setString(1, dateFormat.format(fechaDia));
            ResultSet rs = ps.executeQuery();
            exists = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exists;
    }
}
