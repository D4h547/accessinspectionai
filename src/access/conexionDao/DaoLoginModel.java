package access.conexionDao;

import access.model.ModelLogin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoLoginModel {

    DaoConexion conn;

    public DaoLoginModel(DaoConexion conn) {
        this.conn = conn;
    }

    public int validate(ModelLogin loginModel) throws SQLException {
        int status = 999;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            //String sqlConsult = "SELECT * FROM administradores WHERE correoAdmin = ? AND password = ?";
            String sqlLogin = "SELECT idAdmin FROM administradores WHERE correo = ? AND rol = ? AND pass = aes_encrypt(?,'xyz123');";            
            ps = conn.getConnetion().prepareStatement(sqlLogin);
            //System.out.println(loginModel.toString());
            ps.setString(1, loginModel.getCorreo());
            ps.setString(2, loginModel.getRol());
            ps.setString(3, loginModel.getPassword());
            rs = ps.executeQuery();
            while(rs.next()){
                status = rs.getInt("idAdmin");
            }
            //System.out.println(status);
        } catch (SQLException e) {
            System.out.println("Fallo en la conexion");
            System.out.println(e.getMessage());
        }
        return status;
    }
}