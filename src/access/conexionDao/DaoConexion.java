package access.conexionDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DaoConexion {

    /*
     * HOST: db4free.net 
     * DB: accessinspection 
     * USER: coderoot 
     * PASSWORD: coderoot
     */
    
    // cargamos el driver MySQL
    static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    // Nombre de la Base De Datos
    //static String JDBC_DB = "access_inspection?serverTimezone=UTC";
    static String JDBC_DB = "access_inspection?serverTimezone=UTC";
    // Nombre de Usuario De la Base de Datos
    static String JDBC_USER = "access";//access
    // Contraseña del Usario en la Base de Datos
    static String JDBC_PASS = "inspection";//inspection 025940
    // Url de conexion a la base de datos 
    static String JDBC_URL = "jdbc:mysql://localhost:3306/" + JDBC_DB;
    // variable para guaradar la conexion
    static Connection conn;

    // constructor de la clase
    public DaoConexion() {
        try {
            // Obtenemos el Driver para MySQL
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            if (conn != null) {
                // Si conn es diferente de null es porque se realizao la conexion
                System.out.println("Conexion Database [" + conn + "] OK");
            }
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error en la conexion hacia la base de datos", JOptionPane.ERROR_MESSAGE);
            System.out.println("Fallo en la conexion");
            //System.out.println(e.getMessage());
        }
        //System.err.println("Error al cargar el Driver JDBC");
        //System.err.println(e.getMessage());

    }

    public Connection getConnetion() {
        return conn;
    }

    public void Disconect() {
        System.err.println("Cerrando Conexion... [" + conn + "] OK");
        if (conn != null) {
            try {
                System.err.println("Desconectado de [" + JDBC_DB + "] OK");
                conn.close();
            } catch (SQLException e) {
                System.err.println("Error al desconectar");
                System.err.println(e);
            }
        }
    }

    Object getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
