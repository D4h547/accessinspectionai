package access.inspectionGui;

import access.MaterialSwing.MaterialButton;
import access.MaterialSwing.MaterialColor;
import access.MaterialSwing.MaterialPanelMenu;
import access.conexionDao.DaoConexion;
import access.conexionDao.DaoRegisteDB;
import access.controller.ControllerDeparMuni;
import access.model.ModelEmpleado;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import rojeru_san.componentes.RSDateChooser;

public final class FormRegisterEmployed extends JFrame {

    private static final Color COLOR_FONDO_MARCO = MaterialColor.BLUEGRAY_900;
    private static final Color COLOR_FONT = MaterialColor.WHITH;
    private static final Color COLOR_BORDER_JTEXTFIELD = MaterialColor.LIGHTBLUE_900;
    private static final Color COLOR_FONT_MSG = MaterialColor.DEEP_ORANGE_700;

    private static final Font FONT_TEXT_TITLE = new Font("apple-system", Font.BOLD, 35);
    private static final Font FONT_TEXT_LABELS = new Font("apple-system", Font.BOLD, 18);
    private static final Font FONT_JTEXTFIELDS = new Font("apple-system", Font.PLAIN, 18);
    private static final Font FONT_COMBOBOX = new Font("apple-system", Font.BOLD, 15);
    private static final Font FONT_MSG_ERROR = new Font("apple-system", Font.ITALIC, 14);

    private static final ControllerDeparMuni optDeparMuni = new ControllerDeparMuni();
    private static final WindowsFile jFile = new WindowsFile();

    private MaterialPanelMenu paneMenu;
    private JPanel paneBase;
    private JPanel paneContenedor;
    private JPanel paneTitulo;
    private JPanel paneSegundForm;
    private JPanel paneBtn;

    private JLabel titulo;
    private JLabel textNombre;
    private JLabel textApellido;
    private JLabel textTipoDoc;
    private JLabel textNumDoc;
    private JLabel textFechaNac;
    private JLabel textRH;
    private JLabel textDepar;
    private JLabel textMuni;
    private JLabel textDirec;
    private JLabel textCorreo;
    private JLabel textTelefono;
    private JLabel textCargo;
    private JLabel textFoto;

    private JLabel errTextNombre;
    private JLabel errTextApellido;
    private JLabel errTextFoto;
    private JLabel errTextTele;
    private JLabel errTextCorreo;
    private JLabel errTextCargo;
    private JLabel errTextFecha;
    private JLabel errTextDirec;
    private JLabel errTextNumDoc;
    private JLabel errTextMuni;
    private final JLabel errTextComodin = new JLabel();

    private JTextField ingNombre;
    private JTextField ingApellido;
    private JTextField ingNumDoc;
    private JTextField ingDirec;
    private JTextField ingCorreo;
    private JTextField ingTelefono;
    private JTextField ingCargo;

    private JComboBox ingTipoDoc;
    private JComboBox ingRH;
    private JComboBox ingDepar;
    private JComboBox ingMuni;

    private RSDateChooser ingFechaNac;
    private MaterialButton btnSelectImg;
    private MaterialButton btnSigui;
    private MaterialButton btnSalir;
    int id ;
    DaoConexion conn = new DaoConexion();

    public FormRegisterEmployed() {
        optionWindow();
        initComponents();
    }

    public FormRegisterEmployed(int id) {
        //this.conn = new DaoConexion();
        this.id = id;
        optionWindow();
        initComponents();
    }    
    public FormRegisterEmployed(int id, DaoConexion conn) {
        this.id = id;
        //this.conn = conn;
        optionWindow();
        initComponents();
    }

    public FormRegisterEmployed(ModelEmpleado modelEmpleado, DaoConexion conn) {
        //this.conn = conn;
        optionWindow();
        initComponents();
        backModelEmp(modelEmpleado); 
    }
    public FormRegisterEmployed(ModelEmpleado modelEmpleado) {
        optionWindow();
        initComponents();
        backModelEmp(modelEmpleado); 
    }

    private void initComponents() {

        javax.swing.UIManager.put("OptionPane.font", new Font("apple-system", Font.ITALIC, 20));
        javax.swing.UIManager.put("OptionPane.background", MaterialColor.RED_900);
        javax.swing.UIManager.put("Panel.background", MaterialColor.RED_900);
        javax.swing.UIManager.put("OptionPane.messageForeground", Color.WHITE);

        textNombre = new JLabel("Nombres:");
        textNombre.setForeground(COLOR_FONT);
        textNombre.setFont(FONT_TEXT_LABELS);

        textApellido = new JLabel("Apellidos:");
        textApellido.setForeground(COLOR_FONT);
        textApellido.setFont(FONT_TEXT_LABELS);

        textTipoDoc = new JLabel("Tipo Documento:");
        textTipoDoc.setForeground(COLOR_FONT);
        textTipoDoc.setFont(FONT_TEXT_LABELS);

        textNumDoc = new JLabel("Numero Documento:");
        textNumDoc.setForeground(COLOR_FONT);
        textNumDoc.setFont(FONT_TEXT_LABELS);

        textFechaNac = new JLabel("Fecha de Nacimiento:");
        textFechaNac.setForeground(COLOR_FONT);
        textFechaNac.setFont(FONT_TEXT_LABELS);

        textRH = new JLabel("RH:");
        textRH.setForeground(COLOR_FONT);
        textRH.setFont(FONT_TEXT_LABELS);

        textDepar = new JLabel("Departamento Res:");
        textDepar.setForeground(COLOR_FONT);
        textDepar.setFont(FONT_TEXT_LABELS);

        textMuni = new JLabel("Municipio Res:");
        textMuni.setForeground(COLOR_FONT);
        textMuni.setFont(FONT_TEXT_LABELS);

        textDirec = new JLabel("Direccion Res:");
        textDirec.setForeground(COLOR_FONT);
        textDirec.setFont(FONT_TEXT_LABELS);

        textCorreo = new JLabel("Correo:");
        textCorreo.setForeground(COLOR_FONT);
        textCorreo.setFont(FONT_TEXT_LABELS);

        textTelefono = new JLabel("Telefono:");
        textTelefono.setForeground(COLOR_FONT);
        textTelefono.setFont(FONT_TEXT_LABELS);

        textCargo = new JLabel("Cargo:");
        textCargo.setForeground(COLOR_FONT);
        textCargo.setFont(FONT_TEXT_LABELS);

        textFoto = new JLabel("Foto:");
        textFoto.setForeground(COLOR_FONT);
        textFoto.setFont(FONT_TEXT_LABELS);

        errTextNombre = new JLabel();
        errTextNombre.setForeground(COLOR_FONT_MSG);
        errTextNombre.setFont(FONT_MSG_ERROR);

        errTextApellido = new JLabel();
        errTextApellido.setForeground(COLOR_FONT_MSG);
        errTextApellido.setFont(FONT_MSG_ERROR);

        errTextTele = new JLabel();
        errTextTele.setForeground(COLOR_FONT_MSG);
        errTextTele.setFont(FONT_MSG_ERROR);

        errTextCorreo = new JLabel();
        errTextCorreo.setForeground(COLOR_FONT_MSG);
        errTextCorreo.setFont(FONT_MSG_ERROR);

        errTextCargo = new JLabel();
        errTextCargo.setForeground(COLOR_FONT_MSG);
        errTextCargo.setFont(FONT_MSG_ERROR);

        errTextFoto = new JLabel();
        errTextFoto.setForeground(COLOR_FONT_MSG);
        errTextFoto.setFont(FONT_MSG_ERROR);

        errTextFecha = new JLabel();
        errTextFecha.setForeground(COLOR_FONT_MSG);
        errTextFecha.setFont(FONT_MSG_ERROR);

        errTextNumDoc = new JLabel();
        errTextNumDoc.setForeground(COLOR_FONT_MSG);
        errTextNumDoc.setFont(FONT_MSG_ERROR);

        errTextMuni = new JLabel();
        errTextMuni.setForeground(COLOR_FONT_MSG);
        errTextMuni.setFont(FONT_MSG_ERROR);

        errTextDirec = new JLabel();
        errTextDirec.setForeground(COLOR_FONT_MSG);
        errTextDirec.setFont(FONT_MSG_ERROR);

        ingNombre = new JTextField(15);
        ingNombre.setBorder(null);
        ingNombre.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingNombre, errTextNombre);
        TextFieldKeyEven(ingNombre, errTextNombre);

        ingApellido = new JTextField(15);
        ingApellido.setBorder(null);
        ingApellido.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingApellido, errTextApellido);
        TextFieldKeyEven(ingApellido, errTextApellido);

        ingNumDoc = new JTextField(15);
        ingNumDoc.setBorder(null);
        ingNumDoc.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingNumDoc, errTextNumDoc);

        ingDirec = new JTextField(15);
        ingDirec.setBorder(null);
        ingDirec.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingDirec, errTextDirec);

        ingCorreo = new JTextField(15);
        ingCorreo.setBorder(null);
        ingCorreo.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingCorreo, errTextCorreo);

        ingTelefono = new JTextField(15);
        ingTelefono.setBorder(null);
        ingTelefono.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingTelefono, errTextTele);
        NumTextFieldKeyEven(ingTelefono, errTextTele);

        ingCargo = new JTextField(15);
        ingCargo.setBorder(null);
        ingCargo.setFont(FONT_JTEXTFIELDS);
        TextFieldFocusEvenMsgErro(ingCargo, errTextCargo);
        TextFieldKeyEven(ingCargo, errTextCargo);

        // Grupo de JComboBox
        String[] listDepar = optDeparMuni.getDepartamentos();
        String[] lisTipoDoc = {"VISA", "CC", "CE"};
        String[] lisRH = {"O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+"};

        ingTipoDoc = new JComboBox(lisTipoDoc);
        StylesCombox(ingTipoDoc);
        ingTipoDoc.setBackground(COLOR_FONT);

        ingRH = new JComboBox(lisRH);
        StylesCombox(ingRH);

        ingDepar = new JComboBox(listDepar);
        StylesCombox(ingDepar);

        ingDepar.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                ciudadPorDepar(e);
            }
        });

        ingMuni = new JComboBox();
        StylesCombox(ingMuni);

        ingFechaNac = new RSDateChooser();
        ingFechaNac.setPreferredSize(new Dimension(208, 27));
        ingFechaNac.setColorBackground(COLOR_FONDO_MARCO);
        ingFechaNac.setColorForeground(new Color(0, 0, 0));
        ingFechaNac.setColorSelForeground(Color.BLACK);
        ingFechaNac.setFont(FONT_JTEXTFIELDS);

        btnSelectImg = new MaterialButton("Cargar Imagen");
        btnSelectImg.setPreferredSize(new Dimension(208, 27));
        btnSelectImg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rutaFoto = jFile.JFileChoorsetImg(FormRegisterEmployed.this);
                File fileFoto = new File(rutaFoto);
                if (jFile.fileValid(fileFoto)) {
                    String image_string = jFile.encoder(rutaFoto);
                    
                    empleadoModel.setFoto(image_string);
                    empleadoModel.setRutaFoto(rutaFoto);
                    errTextFoto.setForeground(Color.GREEN);
                    errTextFoto.setText("fotografia cargada");
                } else {
                    errTextFoto.setText("Extencion no valida");
                }
            }
        });

        btnSigui = new MaterialButton("Siguiente");
        btnSigui.setBounds(640, 15, 140, 35);
        btnSigui.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                operaciones();
            }
        });

        btnSalir = new MaterialButton("Salir");
        btnSalir.setBounds(480, 15, 140, 35);
        btnSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                conn.Disconect();
                OptSalir();
                dispose();
            }
        });

        paneBase = new JPanel();
        paneBase.setBackground(COLOR_FONDO_MARCO);
        paneBase.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        FlowLayout layout = (FlowLayout) paneBase.getLayout();
        layout.setVgap(0);

        paneContenedor = new JPanel();
        paneContenedor.setPreferredSize(new Dimension(1050, 650));
        paneContenedor.setBorder(BorderFactory.createEmptyBorder(60, 0, 0, 0));
        paneContenedor.setBackground(MaterialColor.BLUEGRAY_900);

        paneMenu = new MaterialPanelMenu();

        paneTitulo = new JPanel();
        paneTitulo.setBounds(0, 0, 0, 0);
        paneTitulo.setPreferredSize(new Dimension(950, 80));
        paneTitulo.setBackground(COLOR_FONDO_MARCO);

        titulo = new JLabel("Registrar Empleado");
        titulo.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        titulo.setForeground(COLOR_FONT);
        titulo.setFont(FONT_TEXT_TITLE);
        paneTitulo.add(titulo);

        paneSegundForm = new JPanel();
        paneSegundForm.setPreferredSize(new Dimension(1000, 450));
        paneSegundForm.setLayout(new GridBagLayout());
        paneSegundForm.setBackground(COLOR_FONDO_MARCO);
        paneSegundForm.setBorder(BorderFactory.createLineBorder(COLOR_BORDER_JTEXTFIELD, 3, true));

        GridBagConstraints gbc = new GridBagConstraints();
        GridBagConstraints gbcMsgError = new GridBagConstraints();
        gbcMsgError.insets = new Insets(-15, 18, 0, 0);
        gbcMsgError.anchor = GridBagConstraints.NORTHWEST;

        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        //gbc.ipadx = 80; largo
        gbc.ipady = 10;
        // top,left,button,right
        gbc.insets = new Insets(0, 18, 10, 0);
        paneSegundForm.add(textNombre, gbc);
        //
        gbcMsgError.gridx = 0;
        gbcMsgError.gridy = 1;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextNombre, gbcMsgError);
        //
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        paneSegundForm.add(ingNombre, gbc);
        //
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textApellido, gbc);
        //
        gbcMsgError.gridx = 2;
        gbcMsgError.gridy = 1;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextApellido, gbcMsgError);
        //
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        paneSegundForm.add(ingApellido, gbc);
        //
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textTipoDoc, gbc);
        //
        gbcMsgError.gridx = 0;
        gbcMsgError.gridy = 4;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextComodin, gbcMsgError);
        //
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingTipoDoc, gbc);
        //
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textNumDoc, gbc);
        //
        gbcMsgError.gridx = 1;
        gbcMsgError.gridy = 4;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextNumDoc, gbcMsgError);
        //
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingNumDoc, gbc);
        //
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textFechaNac, gbc);
        //
        gbcMsgError.gridx = 2;
        gbcMsgError.gridy = 4;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextFecha, gbcMsgError);
        //
        gbc.gridx = 2;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingFechaNac, gbc);
        //
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textRH, gbc);
        //
        gbcMsgError.gridx = 3;
        gbcMsgError.gridy = 4;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextComodin, gbcMsgError);
        //
        gbc.gridx = 3;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingRH, gbc);
        //
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textDepar, gbc);
        //
        gbcMsgError.gridx = 0;
        gbcMsgError.gridy = 7;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextComodin, gbcMsgError);
        //
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingDepar, gbc);
        //
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textMuni, gbc);
        //
        gbcMsgError.gridx = 1;
        gbcMsgError.gridy = 7;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextMuni, gbcMsgError);
        //
        gbc.gridx = 1;
        gbc.gridy = 8;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingMuni, gbc);
        //
        gbc.gridx = 2;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textDirec, gbc);
        //
        gbcMsgError.gridx = 2;
        gbcMsgError.gridy = 7;
        gbcMsgError.gridwidth = 2;
        gbcMsgError.gridheight = 1;
        //gbcMsgError.fill = GridBagConstraints.BOTH;
        paneSegundForm.add(errTextDirec, gbcMsgError);
        //
        gbc.gridx = 2;
        gbc.gridy = 8;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        paneSegundForm.add(ingDirec, gbc);
        //
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textCorreo, gbc);
        //
        gbcMsgError.gridx = 0;
        gbcMsgError.gridy = 10;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextCorreo, gbcMsgError);
        //
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingCorreo, gbc);
        //
        gbc.gridx = 1;
        gbc.gridy = 9;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textTelefono, gbc);
        //
        gbcMsgError.gridx = 1;
        gbcMsgError.gridy = 10;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextTele, gbcMsgError);
        //
        //
        gbc.gridx = 1;
        gbc.gridy = 11;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingTelefono, gbc);
        //
        gbc.gridx = 2;
        gbc.gridy = 9;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textCargo, gbc);
        //
        gbcMsgError.gridx = 2;
        gbcMsgError.gridy = 10;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextCargo, gbcMsgError);
        //
        gbc.gridx = 2;
        gbc.gridy = 11;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(ingCargo, gbc);
        //
        gbc.gridx = 3;
        gbc.gridy = 9;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(textFoto, gbc);
        //
        gbcMsgError.gridx = 3;
        gbcMsgError.gridy = 10;
        gbcMsgError.gridwidth = 1;
        gbcMsgError.gridheight = 1;
        paneSegundForm.add(errTextFoto, gbcMsgError);
        //
        gbc.gridx = 3;
        gbc.gridy = 11;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        paneSegundForm.add(btnSelectImg, gbc);

        paneBtn = new JPanel();
        paneBtn.setBackground(COLOR_FONDO_MARCO);
        paneBtn.setPreferredSize(new Dimension(780, 55));
        paneBtn.setLayout(null);
        paneBtn.add(btnSalir);
        paneBtn.add(btnSigui);

        //paneContenedor.add(paneTitulo);
        paneContenedor.add(paneSegundForm);
        paneContenedor.add(paneBtn);

        paneBase.add(paneMenu);
        paneBase.add(paneContenedor);
        this.getContentPane().add(paneBase);
        paneBase.updateUI();
    }

    private void optionWindow() {

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setIconImage(new ImageIcon(getClass().getResource("/img/ico-paint32x32.png")).getImage());
        setMinimumSize(new Dimension(980, 720));
        //setVisible(true);
        setResizable(true);
        setTitle("Formulario Registro De Empleado");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                //formLogin.dispose();
                //formAsis.setVisible(false);
            }
            @Override
            public void windowClosing(WindowEvent evt) {
                conn.Disconect();
                OptSalir();
            }
        });
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                int whidth = getWidth();
                paneMenu.setResizeTamaMenu(whidth);
            }
        });

    }

    private void TextFieldFocusEvenMsgErro(JTextField jTextField, JLabel jLabel) {
        Color borderTexfield = new Color(0, 123, 255);
        jTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jTextField.setBorder(BorderFactory.createLineBorder(borderTexfield, 2, true));
                jLabel.setText("");
                errTextFecha.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                jTextField.setBorder(null);
                jLabel.setText("");
                errTextFecha.setText("");
            }
        });
    }

    private void TextFieldKeyEven(JTextField jTextField, JLabel msgLabel) {
        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                if (!Character.isLetter(evt.getKeyChar())
                        && !(evt.getKeyChar() == KeyEvent.VK_SPACE)
                        && !(evt.getKeyChar() == KeyEvent.VK_BACK_SPACE)) {
                    evt.consume();
                    Toolkit.getDefaultToolkit().beep();
                    msgLabel.setText("Escribe letras");
                } else {
                    msgLabel.setText("");
                }
            }
        });
    }

    private void NumTextFieldKeyEven(JTextField jTextField, JLabel jLabel) {
        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                if (!Character.isDigit(evt.getKeyChar())
                        && !(evt.getKeyChar() == KeyEvent.VK_SPACE)
                        && !(evt.getKeyChar() == KeyEvent.VK_BACK_SPACE)) {
                    evt.consume();
                    Toolkit.getDefaultToolkit().beep();
                    jLabel.setText("Escribe Numeros");
                } else {
                    jLabel.setText("");
                }
            }
        });
    }

    private void StylesCombox(JComboBox comboBox) {
        comboBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus); //To change body of generated methods, choose Tools | Templates.
                list.setSelectionBackground(new Color(0, 123, 255));
                list.setFixedCellHeight(20);
                return this;
            }
        });
        comboBox.setPreferredSize(new Dimension(208, 28));
        comboBox.setBackground(COLOR_FONT);
        comboBox.setFont(FONT_COMBOBOX);
        comboBox.setBorder(null);
    }

    private void ciudadPorDepar(ItemEvent evn) {
        if (evn.getStateChange() == ItemEvent.SELECTED) {
            String[] ciudades = optDeparMuni.getCiudades(ingDepar.getSelectedItem().toString());
            ingMuni.setModel(new DefaultComboBoxModel(ciudades));
            errTextMuni.setText("");
        }
    }

    private static final ModelEmpleado empleadoModel = new ModelEmpleado();
    private static final String MSGCAMPNULL = "Este campo no puede estar vacio";

    private boolean ValidaFromString(int cant, String string, JLabel jLabel) {
        boolean status = false;
        String textMuch = "Tamaño de la cadena no valido";
        if (!string.equals("")) {
            if (string.length() > 2 && string.length() < cant) {
                String exString = "[A-Za-zÀ-ÿ\\u00f1\\u00d1 ]{3,150}";
                Pattern pattern = Pattern.compile(exString);
                Matcher matcher = pattern.matcher(string);
                if (matcher.matches()) {
                    status = true;
                } else {
                    jLabel.setText("Escribe Letras");
                }
            } else {
                jLabel.setText(textMuch);
            }
        } else {
            jLabel.setText(MSGCAMPNULL);
        }
        return status;
    }

    private boolean ValidaFromNumbs(String cc, String tipoCC, JLabel jLabel) {
        boolean status = false;
        String textMuch = "Tamaño del numero no valido";
        if (!cc.equals("")) {
            if (cc.length() > 4 && cc.length() < 21) {
                if (tipoCC.equals("CC") || tipoCC.equals("CE")) {
                    String exString = "[0-9]{5,20}";
                    Pattern pattern = Pattern.compile(exString);
                    Matcher matcher = pattern.matcher(cc);
                    if (matcher.matches()) {
                        status = true;
                    } else {
                        status = false;
                        jLabel.setText("Ingrese Numeros");
                    }
                } else {
                    String exString = "[A-Za-z0-9]{5,20}";
                    Pattern pattern = Pattern.compile(exString);
                    Matcher matcher = pattern.matcher(cc);
                    if (matcher.matches()) {
                        status = true;
                    } else {
                        status = false;
                        jLabel.setText("Numero de Documento no Valido");
                    }
                }
            } else {
                jLabel.setText(textMuch);
            }
        } else {
            jLabel.setText(MSGCAMPNULL);
        }
        return status;
    }

    private String RemoveCharNotValidString(String string) {
        string = string.replaceAll("[^a-zA-ZÀ-ÿ\\u00f1\\u00d10-9# -]", "");
        return string;
    }

    private boolean VerifyCorreo(String string) {
        boolean status = false;
        if (string.length() < 100) {
            String strCorreo = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(strCorreo);
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                status = true;
            }
        }
        return status;
    }

    private boolean valiateEmployed(ModelEmpleado empl) {
        return !(empl.getApellidos() == null || empl.getNombres() == null
                || empl.getTipoDoc() == null || empl.getNumeroDoc() == null
                || empl.getDtFechaNac() == null || empl.getRH() == null
                || empl.getDepartamento() == null || empl.getMunicipio() == null
                || empl.getDireccion() == null || empl.getCorreo() == null
                || empl.getTelefono() == null || empl.getCargo() == null
                || empl.getFoto() == null);
    }

    
    DaoRegisteDB registerEmp = new DaoRegisteDB(conn);

    private void operaciones() {
        nombres = ingNombre.getText();
        apellidos = ingApellido.getText();
        tipoDoc = (String) ingTipoDoc.getSelectedItem();
        numDoc = ingNumDoc.getText();
        fechaNac = ingFechaNac.getDatoFecha();
        rh = (String) ingRH.getSelectedItem();
        departamento = (String) ingDepar.getSelectedItem();
        municipio = (String) ingMuni.getSelectedItem();
        direccion = ingDirec.getText();
        correo = ingCorreo.getText();
        telefono = ingTelefono.getText();
        cargo = ingCargo.getText();

        if (registerEmp.CheckEmpExists(numDoc)) {
            JOptionPane.showMessageDialog(FormRegisterEmployed.this, "El Empleado con el No. Documento " + numDoc + " Ya se Encuentra Registrado", "Formulario Registro De Empleados", JOptionPane.ERROR_MESSAGE);
        } else {
            // validando nombres
            if (ValidaFromString(150, nombres, errTextNombre)) {
                empleadoModel.setNombres(nombres);
            }
            if (ValidaFromString(150, apellidos, errTextApellido)) { // validando apellidos
                empleadoModel.setApellidos(apellidos);
            }
            empleadoModel.setTipoDoc(tipoDoc);
            // validando numero de documento
            if (ValidaFromNumbs(numDoc, tipoDoc, errTextNumDoc)) {
                empleadoModel.setNumeroDoc(numDoc);
            } else {
                empleadoModel.setNumeroDoc(null);
            }
            // validando fecha
            if (fechaNac != null) {// validando fecha
                empleadoModel.setDtFechaNac(fechaNac);
            } else {
                errTextFecha.setText(MSGCAMPNULL);
            }
            empleadoModel.setRH(rh);
            empleadoModel.setDepartamento(departamento);
            if (municipio != null) {
                empleadoModel.setMunicipio(municipio);
            } else {
                errTextMuni.setText(MSGCAMPNULL);
            }

            // validando Direccion
            if (!direccion.equals("")) {
                direccion = RemoveCharNotValidString(direccion);
                if (direccion.length() > 4 && direccion.length() < 150) {
                    empleadoModel.setDireccion(direccion);
                } else {
                    errTextDirec.setText("Tamaño del Campo No Valido");
                }
            } else {
                errTextDirec.setText(MSGCAMPNULL);
            }
            // validar correo
            if (!correo.equals("")) {
                if (VerifyCorreo(correo)) {
                    empleadoModel.setCorreo(correo);
                } else {
                    errTextCorreo.setText("Formato del Correo No Valido");
                }
            } else {
                errTextCorreo.setText(MSGCAMPNULL);
            }
            // validar telefono
            if (ValidaFromNumbs(telefono, "CC", errTextTele)) {
                empleadoModel.setTelefono(telefono);
            }
            if (ValidaFromString(20, cargo, errTextCargo)) {// validar cargo
                empleadoModel.setCargo(cargo);
            }
            if (rutaFoto == null) {//foto
                errTextFoto.setText("no se cargo una foto");
            }
            Date fechaActual = new Date();
            empleadoModel.setFechaCreacion(fechaActual);
            empleadoModel.setIdAdminCreater(id);
            //FormAddHuella regisHuella2 = new FormRegisterHuella();
            if (valiateEmployed(empleadoModel)) {
                // ejecutar siguiente ventana a insertar datos
                //conn.Disconect();
                FormRegisterHuella regisHuella = new FormRegisterHuella(empleadoModel);
                regisHuella.setVisible(true);
                dispose();
            } else {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(FormRegisterEmployed.this, "Error en el Formulario", "Registro de empleados",JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void OptSalir() {
        FormRegisterHours formRegisterHours = new FormRegisterHours();
        formRegisterHours.setVisible(true);
        //conn.Disconect();
        //dispose();
    }

    private void backModelEmp(ModelEmpleado empleado) {
        ingNombre.setText(empleado.getNombres());
        ingApellido.setText(empleado.getApellidos());
        ingTipoDoc.setSelectedItem(empleado.getTipoDoc());
        ingNumDoc.setText(empleado.getNumeroDoc());
        ingFechaNac.setDatoFecha(empleado.getDtFechaNac());
        ingRH.setSelectedItem(empleado.getRH());
        ingDepar.setSelectedItem(empleado.getDepartamento());
        ingMuni.setSelectedItem(empleado.getMunicipio());
        ingDirec.setText(empleado.getDireccion());
        ingCorreo.setText(empleado.getCorreo());
        ingTelefono.setText(empleado.getTelefono());
        ingCargo.setText(empleado.getCargo());
        
        this.id = empleado.getIdAdminCreater();
    }

    private static String nombres;
    private static String apellidos;
    private static String tipoDoc;
    private static String numDoc;
    private static Date fechaNac;
    private static String rh;
    private static String departamento;
    private static String municipio;
    private static String direccion;
    private static String correo;
    private static String telefono;
    private static String cargo;
    private static String rutaFoto;
}
