package access.inspectionGui;

import access.MaterialSwing.MaterialButton;
import access.MaterialSwing.MaterialColor;
import access.MaterialSwing.MaterialPanelMenu;
import access.conexionDao.DaoConexion;
import access.conexionDao.DaoRegisteDB;
import access.model.ModelEmpleado;
import access.model.ModelHuellaEmp;
import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.DPFPDataAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPErrorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPErrorEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public final class FormRegisterHuella extends JFrame {

    MaterialButton boton = new MaterialButton("Nombre");
    private static final Color COLOR_FONDO_MARCO = MaterialColor.BLUEGRAY_900;
    Font FONT_TEXT_LABELS = new Font("apple-system", Font.BOLD, 18);
    ModelEmpleado emp;
    ModelHuellaEmp huellaEmp = new ModelHuellaEmp();
    DaoConexion conn = new DaoConexion();
    
    public FormRegisterHuella(ModelEmpleado empleado, DaoConexion conn) {
        //this.conn = conn;
        this.emp = empleado;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Imposible modificar el tema visual", "Lookandfeel inválido.",
                    JOptionPane.ERROR_MESSAGE);
        }
        OptionWindows();
        InitComponet();
    }

    public FormRegisterHuella(ModelEmpleado empleado) {
        this.emp = empleado;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Imposible modificar el tema visual", "Lookandfeel inválido.",
                    JOptionPane.ERROR_MESSAGE);
        }
        OptionWindows();
        InitComponet();
    }

    private void OptionWindows() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setIconImage(new ImageIcon(getClass().getResource("/img/ico-paint32x32.png")).getImage());
        setVisible(true);
        setMinimumSize(new Dimension(1020, 730));
        setVisible(true);
        setResizable(true);
        setTitle("Registrar Huella Empleado");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                formWindowClosing(evt);
            }

            @Override
            public void windowOpened(WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
    }
    private final ImageIcon imgHuellaFaild = new ImageIcon(getClass().getResource("/img/huella-faild.png"));
    private final ImageIcon icoHuellaFaild = new ImageIcon(imgHuellaFaild.getImage().getScaledInstance(100, 90, Image.SCALE_DEFAULT));

    private final ImageIcon imgHuellaValid = new ImageIcon(getClass().getResource("/img/huella-valid.png"));
    private final ImageIcon icoHuellaValid = new ImageIcon(imgHuellaValid.getImage().getScaledInstance(100, 90, Image.SCALE_DEFAULT));

    private void InitComponet() {
        javax.swing.UIManager.put("OptionPane.font", new Font("apple-system", Font.BOLD, 40));
        javax.swing.UIManager.put("OptionPane.background", MaterialColor.RED_900);
        javax.swing.UIManager.put("Panel.background", MaterialColor.RED_900);
        javax.swing.UIManager.put("OptionPane.messageForeground", Color.WHITE);

        paneMenu = new MaterialPanelMenu();

        ImageIcon imgHuellaCap = new ImageIcon(getClass().getResource("/img/huellaBlanca.png"));
        ImageIcon icoHuellaCap = new ImageIcon(imgHuellaCap.getImage().getScaledInstance(230, 230, Image.SCALE_DEFAULT));

        jlImgHuellaCap = new JLabel();
        jlImgHuellaCap.setIcon(icoHuellaCap);

        paneImgHuellaCap = new JPanel();
        paneImgHuellaCap.setPreferredSize(new Dimension(340, 260));
        paneImgHuellaCap.setBorder(BorderFactory.createLineBorder(MaterialColor.RED_900, 3));
        paneImgHuellaCap.setBackground(MaterialColor.BLUEGRAY_900);
        paneImgHuellaCap.add(jlImgHuellaCap);

        jlValidImg1 = new JLabel();
        jlValidImg1.setIcon(icoHuellaFaild);

        jlValidImg2 = new JLabel();
        jlValidImg2.setIcon(icoHuellaFaild);

        jlValidImg3 = new JLabel();
        jlValidImg3.setIcon(icoHuellaFaild);

        jlValidImg4 = new JLabel();
        jlValidImg4.setIcon(icoHuellaFaild);

        this.jlMsg.setFont(FONT_TEXT_LABELS);
        this.jlMsg.setForeground(MaterialColor.WHITH);

        paneValidCap = new JPanel();
        paneValidCap.setPreferredSize(new Dimension(400, 140));
        paneValidCap.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        paneValidCap.setLayout(new GridLayout(1, 4, 0, 0));
        paneValidCap.setBackground(MaterialColor.BLUEGRAY_900);
        paneValidCap.add(jlValidImg1);
        paneValidCap.add(jlValidImg2);
        paneValidCap.add(jlValidImg3);
        paneValidCap.add(jlValidImg4);

        paneContentHuella = new JPanel();
        paneContentHuella.setPreferredSize(new Dimension(400, 440));
        paneContentHuella.setBackground(MaterialColor.BLUEGRAY_900);
        paneContentHuella.add(paneImgHuellaCap);
        paneContentHuella.add(jlMsg);
        paneContentHuella.add(paneValidCap);

        String rutaFoto = emp.getRutaFoto();

        Image imgEmpleIcon = new ImageIcon(rutaFoto).getImage();
        ImageIcon icoEmpleIcon = new ImageIcon(imgEmpleIcon.getScaledInstance(160, 160, Image.SCALE_DEFAULT));

        jlImgFotoEmpl = new JLabel();
        jlImgFotoEmpl.setIcon(icoEmpleIcon);

        paneImgEmp = new JPanel();
        paneImgEmp.setBounds(30, 20, 160, 160);
        paneImgEmp.setBackground(MaterialColor.BLUEGRAY_900);
        paneImgEmp.add(jlImgFotoEmpl);

        jlNombres = new JLabel("Nombres: " + emp.getNombres());
        //jlNombres = new JLabel("Nombres: ");
        jlNombres.setFont(FONT_TEXT_LABELS);
        jlNombres.setForeground(MaterialColor.WHITH);

        jlApellidos = new JLabel("Apellidos: " + emp.getApellidos());
        //jlApellidos = new JLabel("Apellidos: ");
        jlApellidos.setFont(FONT_TEXT_LABELS);
        jlApellidos.setForeground(MaterialColor.WHITH);

        jlTipoDoc = new JLabel("Tipo Doc: " + emp.getTipoDoc());
        //jlTipoDoc = new JLabel("Tipo Doc: ");
        jlTipoDoc.setFont(FONT_TEXT_LABELS);
        jlTipoDoc.setForeground(MaterialColor.WHITH);

        jlNumDoc = new JLabel("No. Documento: " + emp.getNumeroDoc());
        //jlNumDoc = new JLabel("No. Documento: ");
        jlNumDoc.setFont(FONT_TEXT_LABELS);
        jlNumDoc.setForeground(MaterialColor.WHITH);

        jlRh = new JLabel("Rh: " + emp.getRH());
        //jlRh = new JLabel("Rh: ");
        jlRh.setFont(FONT_TEXT_LABELS);
        jlRh.setForeground(MaterialColor.WHITH);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha1 = emp.getDtFechaNac();

        jlFechaNac = new JLabel("Fecha Nacimineto: " + format.format(fecha1));
        //jlFechaNac = new JLabel("Fecha Nacimineto: ");
        jlFechaNac.setFont(FONT_TEXT_LABELS);
        jlFechaNac.setForeground(MaterialColor.WHITH);

        jlCorreo = new JLabel("Correo: " + emp.getCorreo());
        //jlCorreo = new JLabel("Correo: ");
        jlCorreo.setFont(FONT_TEXT_LABELS);
        jlCorreo.setForeground(MaterialColor.WHITH);

        jlTelefono = new JLabel("Telefono: " + emp.getTelefono());
        //jlTelefono = new JLabel("Telefono: ");
        jlTelefono.setFont(FONT_TEXT_LABELS);
        jlTelefono.setForeground(MaterialColor.WHITH);

        paneEmpInfo = new JPanel();
        paneEmpInfo.setBackground(MaterialColor.BLUEGRAY_900);
        paneEmpInfo.setBounds(0, 205, 340, 220);
        paneEmpInfo.setLayout(new GridLayout(8, 1));
        paneEmpInfo.add(jlNombres);
        paneEmpInfo.add(jlApellidos);
        paneEmpInfo.add(jlTipoDoc);
        paneEmpInfo.add(jlNumDoc);
        paneEmpInfo.add(jlRh);
        paneEmpInfo.add(jlFechaNac);
        paneEmpInfo.add(jlCorreo);
        paneEmpInfo.add(jlTelefono);

        paneFormPre = new JPanel();
        paneFormPre.setPreferredSize(new Dimension(350, 440));
        paneFormPre.setBackground(MaterialColor.BLUEGRAY_900);
        paneFormPre.setLayout(null);
        paneFormPre.add(paneImgEmp);
        paneFormPre.add(paneEmpInfo);

        Dimension tamButton = new Dimension(140, 35);
        btnAtras = new MaterialButton("Atras");
        btnAtras.setPreferredSize(tamButton);
        btnAtras.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
                new FormRegisterEmployed(emp).setVisible(true);
                //setVisible(false);
                dispose();
            }
        });
        btnGuardar = new MaterialButton("Guardar");
        btnGuardar.setPreferredSize(tamButton);
        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarHuella();
            }
        });
        paneBtns = new JPanel();
        paneBtns.setBackground(COLOR_FONDO_MARCO);
        paneBtns.setPreferredSize(new Dimension(780, 45));
        layout = (FlowLayout) paneBtns.getLayout();
        layout.setHgap(20);
        paneBtns.add(btnAtras);
        paneBtns.add(btnGuardar);

        paneBoderCont = new JPanel();
        paneBoderCont.setBorder(BorderFactory.createLineBorder(MaterialColor.LIGHTBLUE_900, 3));
        paneBoderCont.setBackground(MaterialColor.BLUEGRAY_900);
        paneBoderCont.setPreferredSize(new Dimension(840, 490));
        layout = (FlowLayout) paneBoderCont.getLayout();
        layout.setVgap(20);
        layout.setHgap(0);
        paneBoderCont.add(paneFormPre);
        paneBoderCont.add(paneContentHuella);

        //paneContenedor.setPreferredSize(new Dimension(950, 650));
        paneContenedor = new JPanel();
        paneContenedor.setPreferredSize(new Dimension(1000, 590));
        paneContenedor.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
        paneContenedor.setBackground(COLOR_FONDO_MARCO);
        paneContenedor.add(paneBoderCont);
        paneContenedor.add(paneBtns);

        paneBase = new JPanel();
        layout = (FlowLayout) paneBase.getLayout();
        layout.setVgap(0);
        layout.setHgap(0);
        paneBase.setBackground(COLOR_FONDO_MARCO);
        paneBase.add(paneMenu);
        paneBase.add(paneContenedor);

        this.getContentPane().add(paneBase);
        paneBase.updateUI();
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                int whidth = getWidth();
                paneMenu.setResizeTamaMenu(whidth);
            }
        });
    }

    private void formWindowOpened(WindowEvent evt) {
        start();
        Iniciar();
        btnGuardar.setEnabled(false);
    }

    private void formWindowClosing(WindowEvent evt) {
        conn.Disconect();
        stop();
    }
    //Varible que permite iniciar el dispositivo de lector de huella conectado
    // con sus distintos metodos.
    private DPFPCapture Lector = DPFPGlobal.getCaptureFactory().createCapture();

    //Varible que permite establecer las capturas de la huellas, para determina sus caracteristicas
    // y poder estimar la creacion de un template de la huella para luego poder guardarla
    private DPFPEnrollment Reclutador = DPFPGlobal.getEnrollmentFactory().createEnrollment();

    //Variable que para crear el template de la huella luego de que se hallan creado las caracteriticas
    // necesarias de la huella si no ha ocurrido ningun problema
    private DPFPTemplate template;
    public static String TEMPLATE_PROPERTY = "template";

    protected void Iniciar() {
        Lector.addDataListener(new DPFPDataAdapter() {
            @Override
            public void dataAcquired(final DPFPDataEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ProcesarCaptura(e.getSample());
                    }
                });
            }
        });
        Lector.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            @Override
            public void readerConnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        jlMsg.setText("Lector Conectado");
                    }
                });
            }

            @Override
            public void readerDisconnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        jlMsg.setText("Lector Desconectado");
                    }
                });
            }
        });
        Lector.addErrorListener(new DPFPErrorAdapter() {
            public void errorReader(final DPFPErrorEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(FormRegisterHuella.this, "Error en el Lector de Huellas" + e.getError());
                    }
                });
            }
        });
    }
    public DPFPFeatureSet featuresinscripcion;
    public DPFPFeatureSet featuresverificacion;

    public void ProcesarCaptura(DPFPSample sample) {
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de inscripción.
        featuresinscripcion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);

        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de verificacion.
        featuresverificacion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

        // Comprobar la calidad de la muestra de la huella y lo añade a su reclutador si es bueno
        if (featuresinscripcion != null)
        try {
            Reclutador.addFeatures(featuresinscripcion);// Agregar las caracteristicas de la huella a la plantilla a crear
            // Dibuja la huella dactilar capturada.
            Image image = CrearImagenHuella(sample);
            DibujarHuella(image);
        } catch (DPFPImageQualityException ex) {
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Error al Crear la Plantilla de la Huella" + ex.getMessage());
            //System.err.println("Error: " + ex.getMessage());
        } finally {
            EstadoHuellas();
            // Comprueba si la plantilla se ha creado.
            switch (Reclutador.getTemplateStatus()) {
                case TEMPLATE_STATUS_READY:	// informe de éxito y detiene  la captura de huellas
                    stop();
                    setTemplate(Reclutador.getTemplate());
                    JOptionPane.showMessageDialog(FormRegisterHuella.this, "La Huella se Creo Correctamete", "Registro de Huella Dactilar", JOptionPane.INFORMATION_MESSAGE);
                    jlMsg.setText("GENERANDO CODIGO PIN...");
                    int codePin = GeneradorCodePin();
                    while (registerEmp.ValidateCodePin(codePin)) {
                        codePin = GeneradorCodePin();
                    }
                    //JOptionPane.showMessageDialog(FormRegisterHuella.this, "CODIGO PIN GENERADO: "+codePin, "Registro de Huella Dactilar", JOptionPane.INFORMATION_MESSAGE);
                    jlMsg.setText("<html>CODIGO PIN GENERADO: <font color=\"#42FF33\">" + codePin+"</font></html>");
                    huellaEmp.setCodePin(codePin);
                    btnGuardar.setEnabled(true);
                    btnGuardar.grabFocus();
                    break;

                case TEMPLATE_STATUS_FAILED: // informe de fallas y reiniciar la captura de huellas
                    Reclutador.clear();
                    stop();
                    setTemplate(null);
                    JOptionPane.showMessageDialog(FormRegisterHuella.this, "La Plantilla de la Huella No Pudo ser Creada, Repita el Proceso", "Registro de Huella Dactilar", JOptionPane.ERROR_MESSAGE);
                    EstadoHuellas();
                    start();
                    break;
            }
        }
    }

    public DPFPFeatureSet extraerCaracteristicas(DPFPSample sample, DPFPDataPurpose purpose) {
        DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        try {
            return extractor.createFeatureSet(sample, purpose);
        } catch (DPFPImageQualityException e) {
            return null;
        }
    }

    public Image CrearImagenHuella(DPFPSample sample) {
        return DPFPGlobal.getSampleConversionFactory().createImage(sample);
    }

    public void DibujarHuella(Image image) {
        jlImgHuellaCap.setIcon(new ImageIcon(image.getScaledInstance(paneImgHuellaCap.getWidth()-20, paneImgHuellaCap.getHeight()-20, Image.SCALE_DEFAULT)));
        repaint();
    }

    public void EstadoHuellas() {
        switch (Reclutador.getFeaturesNeeded()) {
            case 3:
                jlValidImg1.setIcon(icoHuellaValid);
                break;
            case 2:
                jlValidImg2.setIcon(icoHuellaValid);
                break;
            case 1:
                jlValidImg3.setIcon(icoHuellaValid);
                break;
            case 0:
                jlValidImg4.setIcon(icoHuellaValid);
                break;
            default:
                jlValidImg1.setIcon(icoHuellaFaild);
                jlValidImg2.setIcon(icoHuellaFaild);
                jlValidImg3.setIcon(icoHuellaFaild);
                jlValidImg4.setIcon(icoHuellaFaild);
                break;
        }
    }

    public void EnviarTexto(String string) {
        jlMsg.setText(string);
    }

    public void start() {
        Lector.startCapture();
        jlMsg.setText("Utilizando el Lector de Huellas");
    }

    public void stop() {
        Lector.stopCapture();
        jlMsg.setText("No se está usando el Lector de Huellas");
    }

    public DPFPTemplate getTemplate() {
        return template;
    }

    public void setTemplate(DPFPTemplate template) {
        DPFPTemplate old = this.template;
        this.template = template;
        firePropertyChange(TEMPLATE_PROPERTY, old, template);
    }

    DaoRegisteDB registerEmp = new DaoRegisteDB(conn);

    private int GeneradorCodePin() {
        int codePin = 0;
        Random pinn = new Random();
        while (codePin < 100000) {
            codePin = pinn.nextInt(1000000);
        }
        return codePin;
    }

    private void GenerarFileDatosEmp() {
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            try (FileWriter archivo = new FileWriter("C:/Users/Public/Documents/" + emp.getNombres() + "_" + emp.getNumeroDoc() + ".txt")) {
                archivo.write("Nombres: " + emp.getNombres() + "\n");
                archivo.write("Apellidos: " + emp.getApellidos() + "\n");
                archivo.write("No. Documento: " + emp.getNumeroDoc() + "\n");
                archivo.write("Fecha Nacimineto: " + formato.format(emp.getDtFechaNac()) + "\n");
                archivo.write("Codigo Pin: " + huellaEmp.getCodePin() + "\n");
                //archivo.write("foto: "+emp.getFoto());
            }
        } catch (IOException ex) {
            Logger.getLogger(FormRegisterHuella.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void guardarHuella() {
        String cc = emp.getNumeroDoc();
        huellaEmp.setTemplate(template);
        huellaEmp.setNumDoc(cc);
        if (registerEmp.InsertFormEmpleado(emp) && registerEmp.InsertHuellaEmpleado(huellaEmp)) {
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Se Registro el Empleado Correctamente", "Registro de empleados", JOptionPane.INFORMATION_MESSAGE);
            //conn.Disconect();
            GenerarFileDatosEmp();
            int id = emp.getIdAdminCreater();
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Se guardo una copia del codigo pin del empleado " + emp.getNombres(), "Registro de empleados", JOptionPane.INFORMATION_MESSAGE);
            emp.resetModel();
            new FormRegisterEmployed(id).setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(FormRegisterHuella.this, "Error al Registrar el Empleado en Base de Datos", "Registro de empleados", JOptionPane.ERROR_MESSAGE);
        }
    }
    FlowLayout layout;
    private MaterialButton btnGuardar;
    private MaterialButton btnAtras;
    private MaterialPanelMenu paneMenu;
    private JPanel paneBase;
    private JPanel paneContenedor;
    private JPanel paneBoderCont;
    private JPanel paneImgHuellaCap;
    private JPanel paneValidCap;
    private JPanel paneContentHuella;
    private JPanel paneBtns;
    private JPanel paneFormPre;
    private JPanel paneEmpInfo;
    private JPanel paneImgEmp;

    private JLabel jlValidImg1;
    private JLabel jlValidImg2;
    private JLabel jlValidImg3;
    private JLabel jlValidImg4;
    private JLabel jlMsg = new JLabel(" ");

    private JLabel jlImgHuellaCap;
    private JLabel jlImgFotoEmpl;

    private JLabel jlNombres;
    private JLabel jlApellidos;
    private JLabel jlTipoDoc;
    private JLabel jlNumDoc;
    private JLabel jlFechaNac;
    private JLabel jlCorreo;
    private JLabel jlTelefono;
    private JLabel jlRh;
}
