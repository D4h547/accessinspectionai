package access.inspectionGui;

import access.MaterialSwing.MaterialButton;
import access.MaterialSwing.MaterialColor;
import access.MaterialSwing.MaterialPanelReloj;
import access.conexionDao.DaoAsistencias;
import access.conexionDao.DaoConexion;
import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.DPFPDataAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPErrorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPErrorEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import utilites.TextPrompt;

public class FormRegisterHours extends JFrame implements ActionListener {

    private static final Font FONT_TEXT_JL = new Font("Roboto Bold", Font.BOLD, 30);
    private static final Font FONT_TEXT_JL2 = new Font("Roboto Bold", Font.BOLD, 24);

    private final ImageIcon imgHuellaBlanca = new ImageIcon(getClass().getResource("/img/huellaBlanca.png"));
    private final ImageIcon icoHuellaBlanca = new ImageIcon(imgHuellaBlanca.getImage().getScaledInstance(220, 220, Image.SCALE_DEFAULT));

    private final ImageIcon imgHuellaFaild = new ImageIcon(getClass().getResource("/img/huellaRoja.png"));
    private final ImageIcon icoHuellaFaild = new ImageIcon(imgHuellaFaild.getImage().getScaledInstance(220, 220, Image.SCALE_DEFAULT));

    private final ImageIcon imgHuellaValid = new ImageIcon(getClass().getResource("/img/huellaVerde.png"));
    private final ImageIcon icoHuellaValid = new ImageIcon(imgHuellaValid.getImage().getScaledInstance(220, 220, Image.SCALE_DEFAULT));

    public DaoConexion conn = new DaoConexion();

    public FormRegisterHours() {
        this.setUndecorated(true);
        optionWindow();
        InitComponent();
    }
    private final DaoAsistencias registe = new DaoAsistencias(conn);

    private void optionWindow() {
        jlDatosEmpleados = new JLabel();
        jlDatosEmpleados.setForeground(Color.WHITE);
        jlDatosEmpleados.setFont(FONT_TEXT_JL);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setIconImage(new ImageIcon(getClass().getResource("/img/ico-paint32x32.png")).getImage());
        setMinimumSize(new Dimension(980, 720));
        setVisible(true);
        setResizable(true);
        setTitle("Registro Hora de Llegada");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                conn.Disconect();
                stop();
            }

            @Override
            public void windowOpened(WindowEvent evt) {
                Iniciar();
                start();
            }
        });
    }

    private void InitComponent() {
        //javax.swing.UIManager.put("OptionPane.font", new Font("apple-system", Font.ITALIC, 20));
        //javax.swing.UIManager.put("OptionPane.background", MaterialColor.RED_900);
        //javax.swing.UIManager.put("Panel.background", MaterialColor.RED_900);
        //javax.swing.UIManager.put("OptionPane.messageForeground", Color.WHITE);

        Toolkit pantalla = Toolkit.getDefaultToolkit();
        Dimension tamanioPan = pantalla.getScreenSize();
        int alPan = tamanioPan.height;
        int px = getWidth();
        int py = (alPan - 600) / 2;

        panelBtns();
        ingCodePin = new JTextField("");
        ingCodePin.setPreferredSize(new Dimension(150, 40));
        ingCodePin.setFont(FONT_TEXT_JL);
        ingCodePin.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                if (!Character.isDigit(evt.getKeyChar())
                        && !(evt.getKeyChar() == KeyEvent.VK_SPACE)
                        && !(evt.getKeyChar() == KeyEvent.VK_BACK_SPACE)) {
                    evt.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        // placeholder
        TextPrompt pt2 = new TextPrompt("Codigo Pin", ingCodePin);
        pt2.changeAlpha(0.8f);
        pt2.setFont(FONT_TEXT_JL2);
        //pt2.changeStyle(Font.ITALIC);

        paneTetxPin = new JPanel();
        paneTetxPin.setPreferredSize(new Dimension(514, 50));
        paneTetxPin.setBackground(MaterialColor.BLUEGRAY_900);
        paneTetxPin.add(ingCodePin);
        paneTetxPin.setVisible(false);

        imgHuella = new JLabel();
        imgHuella.setIcon(icoHuellaBlanca);

        paneImgHuella = new JPanel();
        paneImgHuella.setPreferredSize(new Dimension(220, 220));
        paneImgHuella.setBackground(MaterialColor.BLUEGRAY_900);
        paneImgHuella.add(imgHuella);
        paneImgHuella.setVisible(true);

        panelReloj = new MaterialPanelReloj();
        panelReloj.setPreferredSize(new Dimension(510, 80));

        paneDatos1 = new JPanel();
        paneDatos1.setPreferredSize(new Dimension(614, 180));
        paneDatos1.setBackground(MaterialColor.BLUEGRAY_900);
        paneDatos1.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        paneDatos1.add(panelReloj);
        paneDatos1.add(jlDatosEmpleados);

        jlMsg = new JLabel();
        jlMsg.setForeground(Color.WHITE);
        jlMsg.setFont(FONT_TEXT_JL2);

        paneDatos2 = new JPanel();
        paneDatos2.setPreferredSize(new Dimension(614, 100));
        paneDatos2.setBackground(MaterialColor.BLUEGRAY_900);
        paneDatos2.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        paneDatos2.add(jlMsg);

        paneConteiner = new JPanel();
        paneConteiner.setPreferredSize(new Dimension(714, 545));
        paneConteiner.setBackground(MaterialColor.BLUEGRAY_900);
        //paneConteiner.setBorder(BorderFactory.createEmptyBorder(25, 0, 0, 0));
        paneConteiner.setBorder(BorderFactory.createLineBorder(MaterialColor.RED_900, 2, true));
        paneConteiner.add(paneDatos1);
        paneConteiner.add(paneImgHuella);
        paneConteiner.add(paneTetxPin);
        paneConteiner.add(panePinBtns);
        paneConteiner.add(paneDatos2);

        int po = px - 200;

        btnPin = new MaterialButton("Ingresar Pin");
        btnPin.setBounds(po, 60, 150, 35);
        btnPin.setVisible(true);
        btnPin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarPin();
            }
        });
        btnHuella = new MaterialButton("Ingresar Huella");
        btnHuella.setBounds(po, 60, 150, 35);
        btnHuella.setVisible(false);
        btnHuella.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarHuella();
            }
        });
        int pRegis = po - 170;
        btnRegis = new MaterialButton("Registrar Empleado");
        btnRegis.setBounds(pRegis, 60, 150, 35);
        btnRegis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegis.setEnabled(false);
                btnHuella.setEnabled(false);
                btnPin.setEnabled(false);
                FormLoginGui vtnLogin = new FormLoginGui(Lector, FormRegisterHours.this, conn);
                vtnLogin.setVisible(true);
            }
        });
        paneBtn = new JPanel();
        paneBtn.setPreferredSize(new Dimension(px, 100));
        paneBtn.setBackground(MaterialColor.BLUEGRAY_900);
        paneBtn.setLayout(null);
        paneBtn.add(btnRegis);
        paneBtn.add(btnPin);
        paneBtn.add(btnHuella);

        paneBase = new JPanel();
        paneBase.setBackground(MaterialColor.BLUEGRAY_900);
        paneBase.setBorder(BorderFactory.createEmptyBorder(py, 0, 0, 0));
        paneBase.add(paneConteiner);
        paneBase.add(paneBtn);

        this.getContentPane().add(paneBase);
        paneBase.updateUI();
    }

    private void IngresarHuella() {
        //stop();
        paneTetxPin.setVisible(false);
        paneImgHuella.setVisible(true);

        btnHuella.setVisible(false);
        btnPin.setVisible(true);

        btnRegis.setVisible(true);

        paneDatos2.setVisible(true);
        panePinBtns.setVisible(false);

        ingCodePin.setText("");
        //Iniciar();
        start();
        //Dilay();
    }

    private void IngresarPin() {
        paneTetxPin.setVisible(true);
        paneImgHuella.setVisible(false);

        btnPin.setVisible(false);
        btnHuella.setVisible(true);

        btnRegis.setVisible(false);

        paneDatos2.setVisible(false);
        panePinBtns.setVisible(true);
        jlDatosEmpleados.setText("Ingrese Su Codigo Pin");
        stop();
    }

    private void panelBtns() {

        btn1 = new MaterialButton("1");
        btn1.addActionListener(this);
        btn2 = new MaterialButton("2");
        btn2.addActionListener(this);
        btn3 = new MaterialButton("3");
        btn3.addActionListener(this);
        btn4 = new MaterialButton("4");
        btn4.addActionListener(this);
        btn5 = new MaterialButton("5");
        btn5.addActionListener(this);
        btn6 = new MaterialButton("6");
        btn6.addActionListener(this);
        btn7 = new MaterialButton("7");
        btn7.addActionListener(this);
        btn8 = new MaterialButton("8");
        btn8.addActionListener(this);
        btn9 = new MaterialButton("9");
        btn9.addActionListener(this);
        btn0 = new MaterialButton("0");
        btn0.addActionListener(this);

        btnIngresar = new MaterialButton("Ingresar");
        btnIngresar.addActionListener(this);
        btnBorrar = new MaterialButton("Borrar");
        btnBorrar.addActionListener(this);

        panePinBtns = new JPanel();
        panePinBtns.setPreferredSize(new Dimension(300, 260));
        panePinBtns.setLayout(new GridLayout(4, 3));
        panePinBtns.setVisible(false);
        panePinBtns.add(btn1);
        panePinBtns.add(btn2);
        panePinBtns.add(btn3);
        panePinBtns.add(btn4);
        panePinBtns.add(btn5);
        panePinBtns.add(btn6);
        panePinBtns.add(btn7);
        panePinBtns.add(btn8);
        panePinBtns.add(btn9);
        panePinBtns.add(btn0);
        panePinBtns.add(btnIngresar);
        panePinBtns.add(btnBorrar);
    }

    //Varible que permite iniciar el dispositivo de lector de huella conectado
    // con sus distintos metodos.
    public final DPFPCapture Lector = DPFPGlobal.getCaptureFactory().createCapture();

    //Varible que permite establecer las capturas de la huellas, para determina sus caracteristicas
    // y poder estimar la creacion de un template de la huella para luego poder guardarla
    private final DPFPEnrollment Reclutador = DPFPGlobal.getEnrollmentFactory().createEnrollment();

    //Esta variable tambien captura una huella del lector y crea sus caracteristcas para auntetificarla
    // o verificarla con alguna guardada en la BD
    private final DPFPVerification Verificador = DPFPGlobal.getVerificationFactory().createVerification();

    //Variable que para crear el template de la huella luego de que se hallan creado las caracteriticas
    // necesarias de la huella si no ha ocurrido ningun problema
    private DPFPTemplate template;
    public static String TEMPLATE_PROPERTY = "template";

    protected void Iniciar() {
        Lector.addDataListener(new DPFPDataAdapter() {
            @Override
            public void dataAcquired(final DPFPDataEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println(GREEN_BOLD + "La Huella Digital ha sido Capturada" + RESET);
                        ProcesarCaptura(e.getSample());
                    }
                });
            }
        });
        Lector.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            @Override
            public void readerConnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        jlDatosEmpleados.setText("Coloque el Dedo Sobre el Lector");
                        //System.out.println("El Sensor de Huella Digital esta Activado o Conectado");
                    }
                });
            }

            @Override
            public void readerDisconnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        //EnviarTexto("El Sensor de Huella Digital esta Desactivado o no Conectado");
                        jlDatosEmpleados.setText("Lector de Huella Desconectado");
                        //System.out.println("Desactivado");
                    }
                });
            }
        });
        Lector.addErrorListener(new DPFPErrorAdapter() {
            public void errorReader(final DPFPErrorEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        //EnviarTexto("Error: " + e.getError());
                        JOptionPane.showMessageDialog(FormRegisterHours.this, "Error En El Lector De Huellas" + e.getError());
                        //System.out.println("Error: " + e.getError());
                    }
                });
            }
        });
    }
    public DPFPFeatureSet featuresinscripcion;
    public DPFPFeatureSet featuresverificacion;

    private void ProcesarCaptura(DPFPSample sample) {
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de inscripción.
        featuresinscripcion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de verificacion.
        featuresverificacion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
        // Comprobar la calidad de la muestra de la huella y lo añade a su reclutador si es bueno
        if (featuresinscripcion != null)
            try {
            identificarHuella(); //System.err.println("Error: " + ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(FormRegisterHours.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private DPFPFeatureSet extraerCaracteristicas(DPFPSample sample, DPFPDataPurpose purpose) {
        DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        try {
            return extractor.createFeatureSet(sample, purpose);
        } catch (DPFPImageQualityException e) {
            return null;
        }
    }

    public void start() {
        Lector.startCapture();
        //jlDatosEmpleados.setText("Coloque el Dedo Sobre el Lector");
    }

    public void stop() {
        Lector.stopCapture();
        //jlDatosEmpleados.setText("Lector de Huella Apagado");
    }

    private DPFPTemplate getTemplate() {
        return template;
    }

    private void setTemplate(DPFPTemplate template) {
        DPFPTemplate old = this.template;
        this.template = template;
        firePropertyChange(TEMPLATE_PROPERTY, old, template);
    }

    private void identificarHuella() throws IOException {
        btnPin.setEnabled(false);
        btnRegis.setEnabled(false);
        try {
            String sqlSelect = "SELECT numDocEmp,huella FROM HuellaCode";
            PreparedStatement identificarStmt = conn.getConnetion().prepareStatement(sqlSelect);
            ResultSet rs = identificarStmt.executeQuery();
            while (rs.next()) {
                byte templateBuffer[] = rs.getBytes("huella");
                String numDoc = rs.getString("numDocEmp");
                //Crea una nueva plantilla a partir de la guardada en la base de datos
                DPFPTemplate referenceTemplate = DPFPGlobal.getTemplateFactory().createTemplate(templateBuffer);
                //Envia la plantilla creada al objeto contendor de Template del componente de huella digital
                setTemplate(referenceTemplate);
                // Compara las caracteriticas de la huella recientemente capturda con la
                // alguna plantilla guardada en la base de datos que coincide con ese tipo
                DPFPVerificationResult result = Verificador.verify(featuresverificacion, getTemplate());
                //compara las plantilas (actual vs bd)
                //Si encuentra correspondencia dibuja el mapa
                //e indica el nombre de la persona que coincidió.
                if (result.isVerified()) {
                    MostrarDatosEmp(numDoc);
                    imgHuella.setIcon(icoHuellaValid);
                    Toolkit.getDefaultToolkit().beep();
                    Reclutador.clear();
                    RegistrarAsistencia(numDoc);
                    //JOptionPane.showMessageDialog(FormRegisterHours.this, "Se Registro La Asistencia ", "Registro De Asistencias", JOptionPane.INFORMATION_MESSAGE);
                    setTemplate(null);
                    stop();
                    pausa();
                    return;
                }
            }
            //Si no encuentra alguna huella correspondiente al nombre lo indica con un mensaje
            setTemplate(null);
            Toolkit.getDefaultToolkit().beep();
            jlDatosEmpleados.setText("");
            jlMsg.setText("<html><font color=\"#F81717\">Huella Dactilar No Registrada</font></html>");
            imgHuella.setIcon(icoHuellaFaild);
            stop();
            pausa();
        } catch (SQLException e) {
            //Si ocurre un error lo indica en la consola
            Toolkit.getDefaultToolkit().beep();
            //System.err.println("Error al identificar huella dactilar." + e.getMessage());
            jlMsg.setText("Error al identificar huella dactilar." + e.getMessage());
            //jlMsg.setText("Error huella dactilar." + e.getMessage());
            imgHuella.setIcon(icoHuellaFaild);
            stop();
            pausa();
        }
    }
    private void ValidateCodePin() {
        String stCodePin = ingCodePin.getText();
        btnHuella.setEnabled(false);
        btnRegis.setEnabled(false);
        paneDatos1.setPreferredSize(new Dimension(614, 240));
        paneTetxPin.setPreferredSize(new Dimension(514, 90));
        if (!stCodePin.equals("")) {
            try {
                int codePin = Integer.parseInt(stCodePin);
                if (registe.ValidateCodePin(codePin)) {
                    panelReloj.setPreferredSize(new Dimension(510, 120));
                    String numDoc = null;
                    String sqlSelectPin = "SELECT numDocEmp FROM HuellaCode WHERE codigoPin = ?";                   
                    try {
                        PreparedStatement ps = conn.getConnetion().prepareStatement(sqlSelectPin);
                        ps.setInt(1, codePin);
                        ResultSet rs = ps.executeQuery();
                        while (rs.next()) {
                            numDoc = rs.getString("numDocEmp");
                        }
                        RegistrarAsistencia(numDoc);
                        MostrarDatosEmp(numDoc);
                        //jlDatosEmpleados.setText("");
                        ingCodePin.setText("");
                        pausa();
                    } catch (SQLException ex) {
                        Logger.getLogger(FormRegisterHours.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    jlDatosEmpleados.setText("");
                    ingCodePin.setText("");
                    jlMsg.setText("<html><font color=\"#F81717\">Codigo Pin No Registrado<font></html>");
                    pausa2();
                }
            } catch (NumberFormatException e) {
                jlDatosEmpleados.setText("");
                ingCodePin.setText("");
                jlMsg.setText("<html><font color=\"#F81717\">No se Ingreso Un Codigo Pin</font></html>");
                pausa2();
            }
        } else {
            jlDatosEmpleados.setText("");
            jlMsg.setText("<html><font color=\"#F81717\">No se Ingreso Un Codigo Pin</font></html>");
            pausa2();
        }
    }

    private void MostrarDatosEmp(String cc) {
        List datos = registe.SelectDatosEmp(cc);
        jlDatosEmpleados.setText("<html> <center> " + datos.get(1).toString() + " " + datos.get(2).toString() + "<br>" + "CC: " + datos.get(0).toString() + "</center> </html>");
    }

    private void MostrarDatosEmp2(String cc) {
        List datos = registe.SelectDatosEmp(cc);
        jlDatosEmpleados.setText("<html> <center> <font color=\"#42FF33\">" + datos.get(1).toString() + " " + datos.get(2).toString() + "<br>" + "CC: " + datos.get(0).toString() + "</font></center> </html>");
    }

    private void MostrarDatosEmpError(String cc) {
        List datos = registe.SelectDatosEmp(cc);
        jlDatosEmpleados.setText("<html> <center> <font color=\"#F81717\">" + datos.get(1).toString() + " " + datos.get(2).toString() + "<br>" + "CC: " + datos.get(0).toString() + "</font></center> </html>");
    }

    private void resetVista() {
        panelReloj.setPreferredSize(new Dimension(510, 80));
        paneDatos1.setPreferredSize(new Dimension(614, 180));
        paneTetxPin.setPreferredSize(new Dimension(514, 50));
        btnPin.setEnabled(true);
        btnRegis.setEnabled(true);
        pausa.stop();
        jlDatosEmpleados.setText("Coloque la Huella Sobre el Lector");
        jlMsg.setText("");
        imgHuella.setIcon(icoHuellaBlanca);
        IngresarHuella();
    }

    private void RegistrarAsistencia(String cc) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        Date fechaActual = new Date();
        String hora = timeFormat.format(fechaActual);
        String fecha = dateFormat.format(fechaActual);
        if (registe.CheckHourEntrada(cc, fecha)) {
            if (registe.CheckHourSalida(cc, fecha)) {
                //JOptionPane.showMessageDialog(FormRegisterHours.this, "Error ya se Registro su hora de Entrada y Salida", "Registro De Asistencias", JOptionPane.ERROR_MESSAGE);
                jlMsg.setText("<html><font color=\"#F81717\">HORAS DE ENTRADA Y SALIDA REGISTRADAS<font></html>");
                imgHuella.setIcon(icoHuellaFaild);
                //System.out.println("Error ya se Registro su hora de Entrada y Salida");
            } else {
                if (registe.InsertHoraSalida(hora, fecha, cc)) {
                    //JOptionPane.showMessageDialog(FormRegisterHours.this, "Se Registro La Asistencia de Salida", "Registro De Asistencias", JOptionPane.INFORMATION_MESSAGE);
                    jlMsg.setText("<html> <font color=\"#62CB06\">SALIDA REGISTRADA" + "<br>" + " <center> HORA: " + hora + "</center> </font> </html>");
                    //System.out.println("Se Registro La Asistencia de Salida");
                } else {
                    //JOptionPane.showMessageDialog(FormRegisterHours.this, "Error Interno al Registrar la asistencia", "Registro De Asistencias", JOptionPane.ERROR_MESSAGE);
                    jlMsg.setText("<html> <font color=\"#F81717\">Error Interno al Registrar la asistencia </font>");
                    imgHuella.setIcon(icoHuellaFaild);
                    //System.out.println("Error Interno al Registrar la asistencia");
                }
            }
        } else {
            if (registe.InsertHoraEntrada(hora, fecha, cc)) {
                //JOptionPane.showMessageDialog(FormRegisterHours.this, "Se Registro La Asistencia de Entrada", "Registro De Asistencias", JOptionPane.INFORMATION_MESSAGE);
                jlMsg.setText("<html> <font color=\"#62CB06\">ENTRADA REGISTRADA" + "<br>" + " <center> HORA: " + hora + "</center> </font></html>");
                //System.out.println("Se Registro La Asistencia de Entrada");
            } else {
                //JOptionPane.showMessageDialog(FormRegisterHours.this, "Error Interno al Registrar la asistencia", "Registro De Asistencias", JOptionPane.ERROR_MESSAGE);
                jlMsg.setText("<html> <font color=\"#F81717\">Error Interno al Registrar la asistencia </font>");
                imgHuella.setIcon(icoHuellaFaild);
                //System.out.println("Error Interno al Registrar la asistencia");
            }
        }
        Toolkit.getDefaultToolkit().beep();
    }

    private void pausa() {
        pausa = new Timer(5000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetVista();
            }
        });
        pausa.setRepeats(true);
        pausa.start();
    }

    private void pausa2() {
        pausa2 = new Timer(3000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                paneDatos1.setPreferredSize(new Dimension(614, 180));
                paneTetxPin.setPreferredSize(new Dimension(514, 50));
                btnHuella.setEnabled(true);
                btnRegis.setEnabled(true);
                ingCodePin.setText("");
                panePinBtns.setVisible(true);
                paneDatos2.setVisible(false);
                jlMsg.setText("");
                jlDatosEmpleados.setText("Ingrese Su Codigo Pin");
                pausa2.stop();
            }
        });
        pausa2.setRepeats(true);
        pausa2.start();
    }

    private Timer pausa, pausa2;
    MaterialPanelReloj panelReloj;
    private JPanel paneBase;
    private JPanel paneTetxPin;
    private JPanel paneConteiner;
    private JPanel paneBtn;
    private JPanel paneDatos1;
    private JPanel paneDatos2;
    private JPanel panePinBtns;
    private JPanel paneImgHuella;
    private JLabel imgHuella;
    private JLabel jlDatosEmpleados;
    private JLabel jlMsg;
    private JTextField ingCodePin;
    public MaterialButton btnRegis, btnHuella, btnPin;
    private MaterialButton btnIngresar, btnBorrar, btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btn1) {
            ingCodePin.setText(ingCodePin.getText() + "1");
        } else if (e.getSource() == this.btn2) {
            ingCodePin.setText(ingCodePin.getText() + "2");
        } else if (e.getSource() == this.btn3) {
            ingCodePin.setText(ingCodePin.getText() + "3");
        } else if (e.getSource() == this.btn4) {
            ingCodePin.setText(ingCodePin.getText() + "4");
        } else if (e.getSource() == this.btn5) {
            ingCodePin.setText(ingCodePin.getText() + "5");
        } else if (e.getSource() == this.btn6) {
            ingCodePin.setText(ingCodePin.getText() + "6");
        } else if (e.getSource() == this.btn7) {
            ingCodePin.setText(ingCodePin.getText() + "7");
        } else if (e.getSource() == this.btn8) {
            ingCodePin.setText(ingCodePin.getText() + "8");
        } else if (e.getSource() == this.btn9) {
            ingCodePin.setText(ingCodePin.getText() + "9");
        } else if (e.getSource() == this.btn0) {
            ingCodePin.setText(ingCodePin.getText() + "0");
        } else if (e.getSource() == this.btnIngresar) {
            panePinBtns.setVisible(false);
            paneDatos2.setVisible(true);
            ValidateCodePin();
        } else if (e.getSource() == this.btnBorrar) {
            ingCodePin.setText("");
        }
    }
}
