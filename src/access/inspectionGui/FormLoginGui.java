package access.inspectionGui;

import access.conexionDao.DaoConexion;
import access.conexionDao.DaoLoginModel;
import access.controller.ControllerVerify;
import access.model.ModelLogin;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import utilites.TextPrompt;

public final class FormLoginGui extends JFrame {

    private static JPanel paneBase;
    private static JPanel paneContenedor;
    private static JPanel paneLogo;
    private static JPanel paneTitulo;
    private static JPanel paneForm;
    private static JLabel msgText;
    private static JLabel imgLogo;
    private static JTextField ingNameUser;
    private static JPasswordField ingPassword;
    private static JButton btnIngresar;
    private static String nameUser;
    private static String password;
    private static ImageIcon logoAccess;

    private static final Color COLOR_FONDO_MARCO = new Color(73, 80, 87); // oscuro
    private static final Color COLOR_FONDO_FORM = new Color(202, 199, 199); // gris 
    private static final Color COLOR_FONDO_BTN = new Color(180, 179, 179); // gris oscuro
    private static final Color COLOR_FONDO_BTN_HOVER = new Color(160, 159, 159); // gris mas oscuro
    private static final Color COLOR_BORDER_BTN = new Color(0, 123, 255); // azul-claro
    private static final Color COLOR_FONT_MSG = new Color(190, 31, 31); // rojo

    private static final Font FONT_TITLE = new Font("Century Gothic", Font.BOLD, 35);
    private static final Font FONT_MSG_ERROR = new Font("apple-system", Font.ITALIC, 16);
    private static final Font FONT_JTEXTFIELDS = new Font("apple-system", Font.PLAIN, 15);

    private final ControllerVerify verifyCorreo = new ControllerVerify();
    private final DPFPCapture Lector1;
    private final FormRegisterHours formHours1;
    DaoConexion conn;

    public FormLoginGui(DPFPCapture Lector, FormRegisterHours formHours, DaoConexion conn) {
        this.Lector1 = Lector;
        this.formHours1 = formHours;
        this.conn = conn;
        opcionesVentana();
        IniciarComponentes();
        eventos();
    }

    public void IniciarComponentes() {

        // Contenedor principal
        paneBase = new JPanel();
        paneBase.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 0));
        paneBase.setBackground(COLOR_FONDO_MARCO);

        // panel del formulario
        paneContenedor = new JPanel();
        paneContenedor.setBackground(COLOR_FONDO_FORM);
        paneContenedor.setPreferredSize(new Dimension(380, 522));
        paneContenedor.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
        //FlowLayout layout = (FlowLayout) paneContenedor.getLayout();
        //layout.setVgap(0);

        // panel del titulo
        paneTitulo = new JPanel();
        paneTitulo.setPreferredSize(new Dimension(320, 100));
        paneTitulo.setBackground(COLOR_FONDO_FORM);

        // panel para el logo
        paneLogo = new JPanel();
        paneLogo.setPreferredSize(new Dimension(300, 170));
        paneLogo.setBackground(COLOR_FONDO_FORM);

        imgLogo = new JLabel();
        logoAccess = new ImageIcon(getClass().getResource("/img/ico.png"));
        ImageIcon icoAccess = new ImageIcon(logoAccess.getImage().getScaledInstance(200, 180, Image.SCALE_DEFAULT));
        imgLogo.setIcon(icoAccess);
        paneLogo.add(imgLogo);

        JLabel texTitulo = new JLabel("<html> &nbsp&nbsp ACCESS<br>INSPECTION </html>");
        texTitulo.setFont(FONT_TITLE);
        paneTitulo.add(texTitulo);

        // panel para los JTextfield
        paneForm = new JPanel();
        paneForm.setPreferredSize(new Dimension(320, 230));
        paneForm.setLayout(null);
        paneForm.setBackground(COLOR_FONDO_FORM);

        msgText = new JLabel("", JLabel.CENTER);
        msgText.setForeground(COLOR_FONT_MSG);
        msgText.setFont(FONT_MSG_ERROR);
        msgText.setBounds(10, -5, 300, 35);

        ingNameUser = new JTextField();
        ingNameUser.setHorizontalAlignment(JTextField.CENTER);
        ingNameUser.setFont(FONT_JTEXTFIELDS);
        ingNameUser.setBorder(null);
        ingNameUser.setBounds(10, 30, 300, 35);

        TextFieldFocusEven(ingNameUser);
        TextFieldKeyEven(ingNameUser, msgText);
        // placeholder
        TextPrompt pt = new TextPrompt("Usuario", ingNameUser);
        pt.changeAlpha(0.8f);
        pt.changeStyle(Font.ITALIC);

        ingPassword = new JPasswordField();
        ingPassword.setHorizontalAlignment(JTextField.CENTER);
        ingPassword.setFont(FONT_JTEXTFIELDS);
        ingPassword.setBorder(null);
        ingPassword.setBounds(10, 90, 300, 35);
        TextFieldFocusEven(ingPassword);

        // placeholder
        TextPrompt pt2 = new TextPrompt("Contraseña", ingPassword);
        pt2.changeAlpha(0.8f);
        pt2.changeStyle(Font.ITALIC);

        btnIngresar = new JButton("Ingresar");
        btnIngresar.setFont(FONT_JTEXTFIELDS);
        btnIngresar.setBounds(10, 160, 300, 35);
        btnIngresar.setBackground(COLOR_FONDO_BTN);
        btnIngresar.setBorder(BorderFactory.createLineBorder(COLOR_BORDER_BTN, 1, true));
        JButtonHover(btnIngresar);

        paneForm.add(msgText);
        paneForm.add(ingNameUser);
        paneForm.add(ingPassword);
        paneForm.add(btnIngresar);

        paneContenedor.add(paneTitulo);
        paneContenedor.add(paneLogo);
        paneContenedor.add(paneForm);

        paneBase.add(paneContenedor);
        this.getContentPane().add(paneBase);
    }

    public void opcionesVentana() {

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setIconImage(new ImageIcon(getClass().getResource("/img/ico-paint32x32.png")).getImage());
        setMinimumSize(new Dimension(570, 670));
        setSize(570, 670);
        setVisible(true);
        setResizable(false);
        setTitle("Login Access Inspection");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                Lector1.startCapture();
                formHours1.btnRegis.setEnabled(true);
                formHours1.btnHuella.setEnabled(true);
                formHours1.btnPin.setEnabled(true);
            }

            @Override
            public void windowOpened(WindowEvent evt) {
                Lector1.stopCapture();
            }
        });
    }

    private void operaciones() {
        nameUser = ingNameUser.getText();
        password = new String(ingPassword.getPassword());
        if (!nameUser.equals("") && !password.equals("")) {
            if (verifyCorreo.VerifyCorreo(nameUser)) {
                try {
                    ModelLogin login = new ModelLogin();
                    login.setCorreo(nameUser);
                    login.setPassword(password);
                    login.setRol("adminUser");
                    DaoLoginModel loginDb = new DaoLoginModel(conn);
                    int id = loginDb.validate(login);
                    //System.out.println("el id: " + id);
                    if (id == 1) {
                        conn.Disconect();
                        FormRegisterEmployed register = new FormRegisterEmployed(id);
                        register.setVisible(true);
                        formHours1.dispose();
                        dispose();
                    } else {
                        msgText.setText("Usuario y/o Contraseña Incorrectos");
                        //System.out.println("faild Login");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(FormLoginGui.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                msgText.setText("Formato de correo no valido");
            }
        } else {
            msgText.setText("Los Campos No pueden estar vacios");
        }
    }

    private void eventos() {
        // Objeto oyente de eventos boton iniciar
        ActionListener iniciarOpt = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Llamamos al metodo operaciones()
                operaciones();
                resetVista();
            }
        };
        // añadimos el evento al boton
        btnIngresar.addActionListener(iniciarOpt);
        // Objeto oyente de eventos boton reiniciar
    }

    private void TextFieldFocusEven(JTextField jTextField) {
        Color borderTexfield = new Color(0, 123, 255);
        jTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jTextField.setBorder(BorderFactory.createLineBorder(borderTexfield, 1, true));
            }

            @Override
            public void focusLost(FocusEvent e) {
                jTextField.setBorder(null);
            }
        });
    }

    private void JButtonHover(JButton jButton) {
        jButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                jButton.setBackground(COLOR_FONDO_BTN);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                jButton.setBackground(COLOR_FONDO_BTN_HOVER);
            }
        });
    }

    private void TextFieldKeyEven(JTextField jTextField, JLabel msgLabel) {
        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                msgLabel.setText("");
            }
        });
    }

    private void resetVista() {
        ingNameUser.setText("");
        ingPassword.setText("");
    }
}
