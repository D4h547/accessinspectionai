package access.inspectionGui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

public class WindowsFile {
    /**
    public WindowsFile() {
        System.out.println(JFileChoorsetImg());
        System.out.println(fileValid(fichero));
     * @param patter
     * @return }**/

    public String JFileChoorsetImg(JFrame patter) {
        String fileName = null;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            fileChooser = new JFileChooser();
            FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.img", ImageIO.getReaderFileSuffixes());
            //Le indicamos el filtro
            fileChooser.setFileFilter(filtro);
            
            //Abrimos la ventana, guardamos la opcion seleccionada por el usuario
            int seleccion = fileChooser.showOpenDialog(patter);
            //Si el usuario, pincha en aceptar
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                //Seleccionamos el fichero
                fichero = fileChooser.getSelectedFile();
                //Ecribe la ruta del fichero seleccionado en el campo de texto
                //System.out.println(fichero.getAbsolutePath());
                fileName = fichero.getAbsolutePath();
            } else {
                fileName = "no se encontro el archivo";
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(WindowsFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fileName;
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    public boolean fileValid(File file) {
        boolean validExt = false;

        String extension = getFileExtension(file);
        String ext3 = extension.replace(".", "").toLowerCase();

        //System.out.println(extension);
        /**
         * String[] extenciones = ImageIO.getReaderFileSuffixes(); for (String
         * ext : extenciones) { if (ext.equals(ext3)) { validExt = true;
         * System.out.println("encontrado " + ext); }else{
         * System.out.println("extencion no valida " + ext); }
        }*
         */
        if (ext3.equals("jpg") || ext3.equals("png")) {
            validExt = true;
        } else {
            System.out.println("extencion no valida " + ext3);
        }
        return validExt;
    }
    public String encoder(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        return base64Image;
    }
    private File fichero;
    private JFileChooser fileChooser;
}
