
package access.model;

import com.digitalpersona.onetouch.DPFPTemplate;


public class ModelHuellaEmp {
    private DPFPTemplate template;
    private int CodePin;
    private String numDoc;

    public DPFPTemplate getTemplate() {
        return template;
    }

    public void setTemplate(DPFPTemplate template) {
        this.template = template;
    }

    public int getCodePin() {
        return CodePin;
    }

    public void setCodePin(int CodePin) {
        this.CodePin = CodePin;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    @Override
    public String toString() {
        return "ModelHuellaEmp{" + "template=" + template + ", CodePin=" + CodePin + ", numDoc=" + numDoc + '}';
    }
    
}
