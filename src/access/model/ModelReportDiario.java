package access.model;

import java.util.Date;

public class ModelReportDiario {

    private Date fechaRepDia;
    private Date hora;
    private int idReporQin;

    public Date getFechaRepDia() {
        return fechaRepDia;
    }

    public void setFechaRepDia(Date fechaRepDia) {
        this.fechaRepDia = fechaRepDia;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public int getIdReporQin() {
        return idReporQin;
    }

    public void setIdReporQin(int idReporQin) {
        this.idReporQin = idReporQin;
    }

    @Override
    public String toString() {
        return "ModelReportDiario{" + "fechaActual=" + fechaRepDia + ", hora=" + hora + ", idReporQin=" + idReporQin + '}';
    }
}
