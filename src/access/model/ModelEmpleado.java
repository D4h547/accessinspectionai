package access.model;

import java.util.Date;

public class ModelEmpleado {

    private String Nombres;
    private String Apellidos;
    private String TipoDoc;
    private String NumeroDoc;
    private Date dtFechaNac;
    private String RH;
    private String Departamento;
    private String Municipio;
    private String Direccion;
    private String Correo;
    private String Telefono;
    private String Cargo;
    private String Foto; 
    private Date FechaCreacion;
    private int IdAdminCreater;
    private String RutaFoto;

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String strApelidos) {
        this.Apellidos = strApelidos;
    }

    public String getTipoDoc() {
        return TipoDoc;
    }

    public void setTipoDoc(String TipoDoc) {
        this.TipoDoc = TipoDoc;
    }

    public String getNumeroDoc() {
        return NumeroDoc;
    }

    public void setNumeroDoc(String NumeroDoc) {
        this.NumeroDoc = NumeroDoc;
    }

    public Date getDtFechaNac() {
        return dtFechaNac;
    }

    public void setDtFechaNac(Date dtFechaNac) {
        this.dtFechaNac = dtFechaNac;
    }

    public String getRH() {
        return RH;
    }

    public void setRH(String RH) {
        this.RH = RH;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String Departamento) {
        this.Departamento = Departamento;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String Foto) {
        this.Foto = Foto;
    }

    public String getRutaFoto() {
        return RutaFoto;
    }

    public void setRutaFoto(String RutaFoto) {
        this.RutaFoto = RutaFoto;
    }

    public Date getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(Date FechaCreacion) {
        this.FechaCreacion = FechaCreacion;
    }

    public int getIdAdminCreater() {
        return IdAdminCreater;
    }

    public void setIdAdminCreater(int IdAdminCreater) {
        this.IdAdminCreater = IdAdminCreater;
    }

    @Override
    public String toString() {
        return "ModelEmpleado{" + "Nombres=" + Nombres + ", Apellidos=" + Apellidos + ", TipoDoc=" + TipoDoc + ", NumeroDoc=" + NumeroDoc + ", dtFechaNac=" + dtFechaNac + ", RH=" + RH + ", Departamento=" + Departamento + ", Municipio=" + Municipio + ", Direccion=" + Direccion + ", Correo=" + Correo + ", Telefono=" + Telefono + ", Cargo=" + Cargo + ", Foto=" + Foto + ", FechaCreacion=" + FechaCreacion + ", IdAdminCreater=" + IdAdminCreater + ", RutaFoto=" + RutaFoto + '}';
    }

    public void resetModel() {
        this.Nombres = null;
        this.Apellidos = null;
        this.TipoDoc = null;
        this.NumeroDoc = null;
        this.dtFechaNac = null;
        this.RH = null;
        this.Departamento = null;
        this.Municipio = null;
        this.Direccion = null;
        this.Correo = null;
        this.Telefono = null;
        this.Cargo = null;
        this.Foto = null;
        this.FechaCreacion = null;
        this.RutaFoto = null;
    }
}
