package access.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ModelReportQuin {

    private int id;
    private Date fechaInit;
    private Date fechaFin;
    private Date hora;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaInit() {
        return fechaInit;
    }

    public void setFechaInit(Date fechaInit) {
        this.fechaInit = fechaInit;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "ModelReportQuin{" + "fechaInit=" + fechaInit + ", fechaFin=" + fechaFin + ", hora=" + hora + '}';
    }
}
